%!TEX root = report.tex
\chapter{Results \& Discussion} \label{sec:results}
This section starts by presenting the datasets used to test the \ambe. We then introduce some measures used in this chapter to describe and explain performance. The last section of this chapter discusses the performance of the AMBE and compares it with the performance of the MBE on the datasets presented earlier.

Throughout this chapter two to three colours are used in the plots. In \cref{ss:3:datasets} the colour red is red is always associated with the first distribution mentioned as being part of that dataset in \autoref{tab:2:datasets}, blue refers to the second distribution and green to the third if present. However in \cref{ss:3:performance} they refer to certain subsets of the data, that are not necessarily the same as the distributions from which the set was sampled. The meaning of these colours is be defined on a plot by plot basis.

	\section{Datasets} \label{ss:3:datasets}
		We compared the performance of the Adapted Modified Breiman Estimator with the performance of the Modified Breiman Estimator that used a normal kernel to be able to better compare the two estimators. 

		We have contrasted the performance of the estimators on a number of simulated datasets with known density fields. Set one and two are adapted from \citeauthor{ferdosi2011comparison}. Each set is three-dimensional and has 2400 points in total. The used datasets are:
		\begin{description}
			\item[Dataset 1] consist of a normal distribution and a random uniform background. Two-thirds of the points in this data set were drawn for the Gaussian, one-third from the uniform background.
			\item[Dataset 2] consists of two Gaussian distributions, each with an equal number of points but with different covariance matrices and centres. One third of the points sampled from this dataset were drawn from uniform random background. 
			\item[Dataset 3] consists of points sampled from three Gaussian distributions with the same mean. The covariance matrices were chosen such that ellipsoids representing the probability density function have the same shape but are aligned along a different axis. 
			\item[Dataset 4] consists of a horizontal wall-like structure with an added uniform background.
			\item[Dataset 5] contains two partially intersecting wall-like structures with an added uniform background.
		\end{description}

		Plots of the datasets can be found in \autoref{fig:2:datasets}, a formal definition of each dataset is presented in \autoref{tab:2:datasets}.

		\input{03_datasetPlot.tex}
		\input{03_datasetTable.tex}

	\clearpage
	\section{Measures}
		This section briefly introduces the measures used in this chapter. For each measure we give a mathematical definition, we also explain what we have used it for and if necessary how its values should be interpreted.

		\subsection{Mean Squared Error}
		%Defintie
		The Mean Squared Error ($\mse$) measures the difference between the densities computed by an estimator and the true densities by taking the average of the squared error. The error of an estimate is defined as the difference between $f(\vec{r})$, the true value, and $\hat{f}(\vec{r})$, the estimated value. Using these definitions, the formal definition of the $\mse$ is:
		\begin{equation}\label{eq:3:mse}
			\mse = \frac{1}{n} \sum_{i = 1}^{N} {(\hat{f}(\vec{r}_i) - f(\vec{r}_i))}^2.
		\end{equation}

		%Waarvoor gebruikt
		We have used the $\mse$ to quantify how well an estimator performs on a dataset or on a subset of a dataset. 
		
		\subsection{Fractional Anisotropy}
		An anisotropic object is directionally dependent, e.g. a sphere is not an\-i\-so\-trop\-ic (i.e. isotropic) meaning its shape is the same no matter from what angle you look at it. A non-degenerate ellipsoid is anisotropic since its shape depends on your perspective. The degree of anisotropy can be described using the eigenvalues $\{\lambda_1, \ldots \lambda_d\}$ of the covariance matrix. The fractional anisotropy ($\fa$) is a measure for the anisotropy. This is defined as:
		\begin{equation}\label{eq:3:fa}
			\fa = \sqrt{\frac{2}{3}} \frac{\sqrt{\sum_{i=1}^{3}{(\lambda_i - \mu)}^2}}{{\lambda_1}^2 + {\lambda_2}^2 + {\lambda_3}^2},
		\end{equation}
		where $\mu$ is the average diffusivity: $\mu = \rfrac{1}{3} \cdot (\lambda_1 + \lambda_2 + \lambda_3)$ \cite{telea2008data}. \\

		We used the fractional anisotropy to quantify the shape of the neighbourhoods used to determine the kernel shape. The $\fa$ ranges between zero and one. If it is zero the diffusion is isotropic, that is to say equally restricted in all directions. This is only the case when all eigenvalues are equal and would indicate that the neighbourhood used was spherical in shape and all points in this neighbourhood were uniformly distributed.

		The other extreme indicates that diffusion occurs along only one axis and is fully restricted along the others. This would mean that the used neighbourhood consisted of points on a line in the direction of the eigenvector associated with the only non-zero eigenvalue. 

		It should be noted that when we talk about the $\fa$ of a point we mean the fractional anisotropy of that point and its $k - 1$ neighbours.

		\subsection{Physical density}
		%Definitie
		We define the physical density as the number of observations per unit volume. For example the physical density of 300 points in a cube with edges of length 10 is: $\rfrac{300}{10^3}$. 

		We have used this measure to compare the number of points available for density estimation between the different sets and subsets of datasets.

		\subsection{Interquartile Range}
		% Definitie
		The interquartile range measures the dispersion of the values in a set. The interquartile range of a one-dimensional set $q$ is defined as:
		\begin{equation}
			\iqr(q) = P_{75} - P_{25}.
		\end{equation}
		Where $P_{25}$ is the first quartile and $P_{75}$ the third of set $q$.

		% Waarvoor gebruikt
		We have used the interquartile range to describe the fractional anisotropies of a set of points, since the mean is not always meaningful, after all two sets with the same mean are not necessarily comparative.

	\section{Performance} \label{ss:3:performance}
		This section presents and discusses the results of both estimators, per set. The results can also be found in \autoref{tab:3:results}.

		Each subsection discusses the result of the two estimators on one of the data set. For each dataset estimator combination a plot of the true versus the estimated density is shown. These graphs also show the true densities plotted against the true densities to indicate the performance of a perfect estimator. Each dataset consisted of 4800 points.

		\input{03_resultsTable.tex}

		\subsection{Dataset 1}
			\label{ss:set1}
			\begin{figure}[t]
				\centering
				\begin{subfigure}[b]{0.49\textwidth}
					\centering
					\includegraphics[width=\textwidth]{./img/03_AMBE_result_N2400_01.png}
					\caption{AMBE}
					\label{subfig:3:AMBEresult01}
				\end{subfigure}
				\begin{subfigure}[b]{0.49\textwidth}
					\centering
					\includegraphics[width=\textwidth]{./img/03_MBE_result_N2400_01.png}
					\caption{MBE}
					\label{subfig:3:MBEresult01}
				\end{subfigure}	
				\caption{The results of the AMBE and the MBE on dataset one. Results from subset $A$ are shown in red, results from $\overline{A}$ in blue.}
				\label{fig:3:restulsSet1}
			\end{figure}

			%Verwachting
			We did not expect that one of the estimators would perform better than the other on this dataset, since it did not contain any structures of which we expected that shape-adapted kernels would be advantageous.

			% MSE
			Comparing the Mean Squared Error of the two estimators we see that the \ambe ($\mse = \num{2.201e-09}$) outperforms the \mbe ($\mse = \num{3.125e-09}$). We have separated set one into two subsets: set $A$ contains all points from the intersection of the two distributions ($(x, y, z) \in [10, 40]$), set $\overline{A}$ is the complement of $A$. The results of the two estimators on this dataset are shown in \autoref{fig:3:restulsSet1}. These plots show that both the \mbe and the \ambe oversmoothed, though the AMBE did so to a lesser extent than the MBE. 

			Comparing the $\mse$ of the estimates we find that the mean squared error of the densities computed with the AMBE are $\num{2.924e-09}$ and $\num{1.034e-11}$ for respectively $A$ and $\overline{A}$. Whereas these are $\num{3.722e-09}$ and $\num{1.316e-09}$ when the \mbe is used. These differences indicate that both estimators handled the background around the normal distribution better than the points drawn from the Gaussian.

			% FA
			The mean fractional anisotropy of the points in $A$ is lower (\num{0.16306}) than the average $\fa$ of $\overline{A}$ (\num{0.25075}). Comparing the interquartile range of the $\fa$ of the intersection $(\iqr = \num{0.07560632})$ and its complement $(\iqr = \num{0.1156234})$ we find that the kernels in $\overline{A}$ were reshaped more than those in $A$.

			% Conclusie
			The difference in fractional anisotropy between $A$ and $\overline{A}$ combined with the difference in $\mse$ on $\overline{A}$ between AMBE and MBE suggest that the shape-adapted kernels improved the performance of the density estimator on this dataset.

		\subsection{Dataset 2}
			\begin{figure}
				\centering
				\begin{subfigure}[b]{0.49\textwidth}
					\centering									
					\includegraphics[width=\textwidth]{./img/03_AMBE_result_N2400_02}
					\caption{AMBE}
					\label{subfig:3:AMBEresult02}
				\end{subfigure}
				\begin{subfigure}[b]{0.49\textwidth}
					\centering
					\includegraphics[width=\textwidth]{./img/03_MBE_result_N2400_02}
					\caption{MBE}
					\label{subfig:3:MBEresult02}
				\end{subfigure}	
				\caption{The results of the AMBE and the MBE on dataset two. Results from subset $A$ are shown in red, results from $B$ in blue, and results from $C$ in green.}
				\label{fig:3:restulsSet2}
			\end{figure}		


			% Verwachting
			Based on the results of \nameref{ss:set1} we expected the AMBE to outperform the MBE, especially on the data points between the two spheres.

			% MSE
			The mean squared errors show that the AMBE $(\mse = \num{1.577e-07})$ outperformed the MBE $(\mse = \num{1.821e-07})$. We discuss performance on this dataset using the set $A$: all points from the intersection of the uniform data and the first normal distribution ($(x, y, z) \in [4, 20.5]$), set $B$: all points from the intersection of the uniform data and the second Gaussian ($(x, y, z) \in [20.7, 44.8]$), and set $C$: $\overline{A \cup B}$. 

			The plots in \autoref{fig:3:restulsSet1} show the result per subset. Although the estimators performed comparably, both oversmoothed the density estimate, it seems as though the estimated densities vary more when the AMBE is used.

			The AMBE outperformed the MBE on both set $A$ $(\mse_{AMBE} = \num{4.247e-07}$, $\mse_{MBE} = \num{4.958e-07})$ and set $B$ $(\mse_{AMBE} = \num{3.329e-08}$, $\mse_{MBE} = \num{3.364e-08})$, whereas the MBE achieved better results on the background that did not intersect with either of the two normal distributions $(\mse_{AMBE} = \num{6.095e-11}$, $\mse_{MBE} = \num{4.767e-11})$. \\

			%FA
			Since the mean and $\iqr$ of the $\fa$ of set $A$ ($M = \num{0.16388}, \iqr = \num{0.07077095}$) and $B$ ($M = \num{0.16985}, \iqr = \num{0.07503002}$) do not differ by much we can conclude that in general the kernels for set $A$ were reshaped as strongly as those for set $B$. The difference in performance between these two sets can thus not be completely explained by a difference in kernels.

			The $\fa$ of the data points in set $C$ $(M = \num{0.27443}, \iqr = \num{0.124267})$ is comparable to the fractional anisotropy of the complement of the intersection in set one.

			% Conclusie
			Set $A$ and set $B$ have approximately the same number of points, respectively 820 and 908. However the physical density in set $A$ $(\num{0.1825417})$ is nearly three times as high as in set $B$ $(\num{0.06486863})$. It seems as though a higher physical density is detrimental to the performance of both estimators. 

			The AMBE performed better than the MBE on both set $A$ and set $B$, however the MBE handled the background in this set better than the AMBE, which contradicts what we found with set one. We discuss this difference in \cref{ss:3:generalDiscussion} where we can also consider how the estimators handled the background in set four and five.

		\subsection{Dataset 3}		
			% Verwachting
			We expected that the AMBE would achieve better results than the the MBE since this is the type of dataset where one would expect the shape-adapted kernels to offer a distinctive advantage.

			\begin{figure}[ht]
				\centering
				\begin{subfigure}[b]{0.49\textwidth}
					\centering
					\includegraphics[width=\textwidth]{./img/03_AMBE_result_N2400_03}
					\caption{AMBE}
					\label{subfig:3:AMBEresult03}
				\end{subfigure}
				\begin{subfigure}[b]{0.49\textwidth}
					\centering
					\includegraphics[width=\textwidth]{./img/03_MBE_result_N2400_03}
					\caption{MBE}
					\label{subfig:3:MBEresult03}
				\end{subfigure}	
				\caption{The results of the AMBE and the MBE on dataset three. Results from subset $A$ are shown in red, results from $\overline{A}$ in blue.}
				\label{fig:3:restulsSet3}
			\end{figure}

			% MSE
			The differences between \autoref{subfig:3:MBEresult03} and \autoref{subfig:3:AMBEresult03} show that the MBE gives a pretty good estimate whereas the density estimated by the AMBE is undersmoothed. This is reflected in the mean squared errors which are \num{1.630e-05} and \num{2.668e-06} for respectively the \ambe and the \mbe. We discuss the results of this dataset by comparing the data points in the intersection ($(x, y, z) \in [12, 18]$) of the three distributions, set $A$ and the relative complement of this set in dataset three: $\overline{A}$.

			Comparing the performance of the \ambe on set $A$ $(\mse = \num{1.919e-05})$ and $\overline{A}$ $(\mse = \num{1.058e-05})$ we find that this estimator handled the data points outside of the intersection better than those in set $A$. The opposite is true for the \mbe, the density estimates of the points in set $A$ $(\num{2.342e-06})$ had a lower $\mse$ than those of observations in set $\overline{A}$ $(\num{3.309e-06})$.

			The shape of the data points in set $A$, when plotted as we have plotted the datasets in \autoref{fig:2:datasets}, resemble a sphere, comparative to the shape of the Gaussians in the scatter plots of set one and two. One would thus not expect the AMBE to perform worse than the MBE on set $A$. Comparing the physical density of set $A$ $(\num{7.37037})$ in this dataset with the physical density of the areas of the normal distributions in set two we find that it is extremely high in comparison. This may contribute to the bad performance.

			The fact that the performance difference between the two subsets when the MBE is used, is larger than when the \ambe is used suggests that AMBE's difference in performance is at least partially due to shape-adapted kernels.

			% FA
			The fractional anisotropies of set $A$ $(M = \num{0.1648}$, $\iqr = \num{0.07535079})$ and its relative complement $(M = \num{0.2302}$, $\iqr = \num{0.1112426})$ suggest that although the kernels used for $\overline{A}$ where reshaped more than those used for $A$, neither set used strongly reshaped kernels.

			% Conclusie
			The difference in performance on set $A$ could be due to the high physical density. This this does not however explain why the densities computed by the \mbe on set $\overline{A}$ are closer to the true densities when compared with the densities computed by AMBE. A different reason for this result might be that the method used to compute the local bandwidth parameter cannot handle these types of data sets very well. Another possible explanation is that the true densities of the points in this set are somehow incorrect. 

		\subsection{Dataset 4}
			\begin{figure}
				\centering
				\begin{subfigure}[b]{\textwidth}
					\centering
					\includegraphics[width=\textwidth]{./img/03_AMBE_result_N2400_04.png}
					\caption{AMBE}
					\label{subfig:3:AMBEresult04}
				\end{subfigure}

				\vspace{0.5cm}

				\begin{subfigure}[b]{\textwidth}
					\centering
					\includegraphics[width=\textwidth]{./img/03_MBE_result_N2400_04.png}
					\caption{MBE}
					\label{subfig:3:MBEresult04}
				\end{subfigure}	
				\caption{The results of the AMBE and the MBE on dataset four. Results from subset $A$ are shown in red, results from $\overline{A}$ in blue.}
				\label{fig:3:restulsSet4}
			\end{figure}	

			The results of this dataset, shown in \autoref{fig:3:restulsSet4} are plotted differently than those of set one through three. We have chosen for this representation since the true density of observations in this dataset is one of two values: \num{0.0008962963} or \num{7.407407e-06}. The dashed lines represent these two values in respectively red and blue.

			We have separated the data based on the true density of the points. The resulting two sets correspond to the intersection of the two distributions (set $A$), with true density \num{0.0008962963} and the complement of this intersection, set $\overline{A}$ with true density  \num{0.0008962963}. Each solid line in \autoref{fig:3:restulsSet4} indicates a density estimation for a point in one of the two subsets.

			%Verwachting
			The expectation was that the AMBE would outperform the MBE, since the we shape-adapted kernels should handle the wall-like structure better.

			% MSE
			Comparing \autoref{subfig:3:AMBEresult04} and \autoref{subfig:3:MBEresult04} it seems as though the AMBE outperforms the MBE, since the densities of the AMBE at least lie around the true density. Whereas the density estimated using the \mbe never get near the true densities. The $\mse$ confirms that in general the densities computed with the AMBE ($\mse = \num{6.241e-08}$) are near the true density than those computed with the MBE ($\mse = \num{2.156e-07}$). 

			Although \autoref{subfig:3:AMBEresult04} seems to indicate that the AMBE handled set $A$ better than $\overline{A}$, since the red bars surround the true density whereas the blue bars never quite reach their true density, the Mean Squared Errors, respectively \num{7.616e-08} and \num{5.790e-09}, show the opposite. The MBE shows the same difference in performance between set $A$ and $\overline{A}$ with Mean Squared Errors of respectively \num{2.628e-07} and \num{2.130e-08}. 

			It is probable that the both the AMBE and the MBE estimate the density of observations in $\overline{A}$ that lie near to the wall higher than the density of points farther away from this high density area, we call this the leakage effect. This effect explains why the estimated densities of set $\overline{A}$ differ so much even tough the data were distributed uniformly. The shape-adapted kernels are less influenced by this leakage effect than the kernels used by the \mbe. 
			
			% Fa
			The differences between the fractional anisotropies of the two subsets are quite large. The observations in set $A$ have a mean $\fa$ of \num{0.5060} ($\iqr = \num{0.04049876}$) which indicates that the kernels where reshaped quite strongly and that each kernel was shaped to approximately the same extent as the other kernels used for observations in this subset. Contrary to this the kernels used for the points in set $\overline{A}$ were reshaped to a lesser extent ($M = \num{0.3840}$) but how much each kernel was reshaped differed thrice as much as in set $A$ ($\iqr = \num{0.1351724}$).
			% Conclusie	
			For this dataset the shape-adapted kernels are clearly advantageous.

		\subsection{Dataset 5}
			\begin{figure}
				\centering
				\begin{subfigure}[b]{\textwidth}
					\centering
					\includegraphics[width=\textwidth]{./img/03_AMBE_result_N2400_05.png}
					\caption{AMBE}
					\label{subfig:3:AMBEresult05}
				\end{subfigure}

				\vspace{0.5cm}

				\begin{subfigure}[b]{\textwidth}
					\centering
					\includegraphics[width=\textwidth]{./img/03_MBE_result_N2400_05.png}
					\caption{MBE}
					\label{subfig:3:MBEresult05}
				\end{subfigure}	
				\caption{The results of the AMBE and the MBE on dataset two. Results from subset $A$ are shown in red, results from $B$ in blue, and results from $C$ in green.}
				\label{fig:3:restulsSet5}
			\end{figure}

			The results of this dataset, see \autoref{fig:3:restulsSet5}, are shown in the same type of plot as the results of set four, since the observations in this set have one of three densities. Points in set $A$ have density \num{0.0005016}. These points can be found in one of the wall-like structures where the walls do not intersect. Set $B$ contains all points with density \num{0.001002}, these observations are sampled from the intersection of the two walls. The observations drawn from the uniform background, with true density \num{1.6e-06}, from set $C$. \\

			%Verwachting
			Based on the results of set four we expected that the \ambe would perform better than the MBE. 

			% MSE
			The plots in \autoref{fig:3:restulsSet5} do not show enough difference to confirm that using shape-adapted kernels would be advantageous. The $\mse$ however does show that the AMBE (\num{3.299e-08}) outperformed the MBE (\num{4.714e-08}), though not by much. \autoref{subfig:3:AMBEresult05} and \autoref{subfig:3:MBEresult05} do show that the densities estimated by the AMBE for the observations in set $C$ ($\mse = \num{9.322e-10}$) are nearer to the true density of this set than those estimated by the MBE ($\mse = \num{6.498e-09}$). The effect that the densities computed with the AMBE have a larger range than those determined with the MBE, that we found in all previous datasets is once again present. The densities computed by the \ambe have a smaller $\mse$ than those computed by the MBE for both set $A$ ($\mse_{AMBE} = \num{2.852e-08}$, $\mse_{MBE} = \num{4.178e-08}$) and $B$ ($\mse_{AMBE} = \num{1.535e-07}$, $\mse_{MBE} = \num{1.971e-07}$). 

			We find once again that both estimators perform best on the background, set $C$. It is possible that the results on set $B$ have been negatively influenced by the small number of points in that subset, especially since these points were surround by higher density points of set $A$.

			% Fa
			Comparing the mean fractional anisotropies of the subset we find that the kernels used for set $A$ ($M = \num{0.39064}$, $\iqr = \num{0.153136}$) were reshaped to greater degree than those of set $B$ ($M = \num{0.27451}$, $\iqr = \num{0.1035921}$) and $C$ ($M = \num{0.32939 }$, $\iqr = \num{0.1546229}$). The degree to which the kernels were reshaped within set $A$ and $C$ is quite varied, as indicated by the interquartile ranges. \\

			% Conclusie
			These results show that the densities computed by the AMBE are on average nearer to the true densities than those computed by the MBE. Although the difference between the two estimators is not as large is was for set four, possible because this set is more complex than set four.

	\subsection{General Discussion}
	\label{ss:3:generalDiscussion}
	This section discusses how the results of the different datasets relate to each other. We refer to subset $A$ of dataset four as the set $4.A$.

	%Background
	Every set, expect set three, contained uniform background to some extent. For each set it was true that both estimators performed beter on the subset with the background than on other subsets. Furthermore, in each dataset, expect set the AMBE outperformed the MBE on the subset with background. 

	In \cref{fig:3:restulsSet2} we have suggested that a high physical density might be disadvantageous, independent of the estimator. It is indeed the case that set $2.C$ has a higher physical density than any other subset with background. However since the difference in physical density between set two and one is quite small, \num{0.005376} and \num{0.00476} respectively, it seems improbable that the physical density caused the difference in performance.

	Another possible explanation could be the high average fractional anisotropy of the background of set two, however the background of set five has a higher mean $\fa$ and a larger $\iqr$. Thus neither fractional anisotropy nor physical density seem to explain the difference in performance on these subsets.

	% Gaussians and variance
	Comparing the results of both the AMBE and the MBE of all sets with a Gaussian ($1.A$, $2.A$ and $2.B$) we find that the larger the variance of the Gaussian, the smaller the $\mse$ of the subset with the Gaussian. The physical density of these subsets, \num{0.01444}, \num{0.00656} and \num{0.012736} for respectively set $1.A$, $2.A$ and $2.B$, does not seem to influence performance. \\

	% Set 3
	The most surprising result was that the AMBE did not perform better than the MBE on set three, even though we expected it to. This could be attributed to the absence of background, however in the data sets with background, the other subsets, except $2.C$, also performed consistently better with the AMBE. Another possible reason is that the plug-in estimator did not handle cannot handle these kind of shapes very well.