%!TEX root = report.tex
\chapter{Introduction}
	\label{sec:introduction}
	Kernel density estimation is an important tool in many fields. Computer scientists use it for machine learning \cite{scholkopf2001estimating}, pattern recognition \cite{fukunaga1975estimation}, image analysis \cite{mittal2004motion} and computer vision \cite{chen2002robust} to name a few applications. It has also been used by archaeologists to cluster excavated pieces of Roman waste glass \cite{baxter1997some}. Neuroscientists have used it to estimate the discharge rate of a neuron \cite{shimazaki2010kernel}. In astronomy, estimating densities is the first step for a lot of research, e.g. for reconstruction of the large-scale structure of the universe, an estimate of the cosmic density field is essential \cite{ferdosi2011comparison}. \Cref{fig:1:astronomy} shows part of a simulation of the universe and in particular the density distribution of dark matter. On a large-scale, the cosmos consists of high density structures separated by low density immense voids \cite{falckMillenium}. Astronomers use density estimation to identify these voids and structures.

	\begin{figure}
		\centering
		\includegraphics[width=\textwidth]{./img/01_astronomy}
		\caption{A slice of a simulation of the universe, the bright yellow regions indicate high density clusters. Image adapted from \cite{falckMillenium}.}
		\label{fig:1:astronomy}
	\end{figure}

	Density estimation can be formulated as the following problem: given $n$ points, $x_1, \ldots, x_n$, drawn independently from some, unknown, density $f(x)$, estimate the density function $f(x)$ \cite{breiman1977variable}.

	Two different families of density estimators exist: non-parametric and parametric. The parametric estimators assume knowledge of the type of the distribution of $f(x)$, whereas the non-parametric estimators do not rely on such assumptions \cite{ferdosi2011comparison}.

	In this work we consider a method for kernel density estimation that is sensitive to the shape of the local distribution. \Cref{sec:method} discusses the proposed kernel density estimator in detail. 

	This chapter first considers the histogram as a density estimator which is followed by a discussion of the main concepts of kernel density estimation necessary for this paper. \Cref{s:1:kdes} uses this knowledge to review several kernel density estimation methods.

	We use the following notation:
	\begin{itemize}
		\item $\norm{d}{\mu}{\Sigma}$ denotes a $d$-dimensional normal distribution with the mean $\mu$. This mean can be denoted as a $d$-dimensional vector, or as a scalar. If it is a scalar it should be interpreted as a $d$-dimensional vector with the scalar value in all dimensions. The last parameter, $\Sigma$, denotes the standard deviation of a univariate distribution or the covariance matrix in the multivariate case.
		\item $\uni{d}{\vec{a}}{\vec{b}}$ denotes the uniform distribution in $d$ dimensions, with lower bounds given by the vector $\vec{a}$ and upper bounds by $\vec{b}$, both vectors have length $d$.
		\item $\diag(A)$ represents a diagonal matrix. The parameter $A$ can be a scalar or a vector. If $A$ is a scalar the dimension of the matrix depends on the dimension $d$ of the distribution. In the resulting $d \times d$ matrix, $A$ is placed at each diagonal element. If $A$ is a vector of length $q$, the matrix is $q \times q$, and has the elements of $A$ on its diagonal. E.g. $\diag([a,b])$ denotes the matrix:
		\begin{equation*}
			\begin{bmatrix}
				a & 0\\
				0 & b\\
			\end{bmatrix}.
		\end{equation*}
		\item $\vec{r}$ represents a vector named $r$.
	\end{itemize}	

		\begin{figure}[t]
			\centering
			\begin{subfigure}[b]{0.3\textwidth}
				\centering
				\includegraphics[width=\textwidth]{./img/01_hist_01}
				\caption{bin size = 1.5}
				\label{subfig:1:histogram:01}
			\end{subfigure}
			\begin{subfigure}[b]{0.3\textwidth}
				\centering
				\includegraphics[width=\textwidth]{./img/01_hist_02}
				\caption{bin size = 1.6}
				\label{subfig:1:histogram:02}
			\end{subfigure}	
			\begin{subfigure}[b]{0.3\textwidth}
				\centering
				\includegraphics[width=\textwidth]{./img/01_hist_03}
				\caption{bin size = 1.9}
				\label{subfig:1:histogram:03}
			\end{subfigure}			
			\caption{Three histograms of a single distribution, which contains 100 values between 60 and 84. The histograms only differ in bin size. Adapted from \textcite{cohen1995empirical}.}
			\label{fig:1:histogram}
		\end{figure}

	\section{Histogram}
		The best known non-parametric density estimator, though most might not know it as such, is the histogram. After all its shape is indicative of the shape of the distribution that the shown data were drawn from. Although histograms are useful for exploration and presentation of one-dimensional data, they should not be used for multidimensional data or complex procedures that require density estimation, such as cluster analysis \cite{silverman1986density}. The second point is best illustrated with \autoref{fig:1:histogram}. Depending on the bin size the histograms suggest (\ref{subfig:1:histogram:01}) a distribution with gaps, (\ref{subfig:1:histogram:02}) a distribution that is almost bell shaped or, (\ref{subfig:1:histogram:03}) a heavily skewed distribution \cite{cohen1995empirical}. Using histograms for multidimensional data is not preferable due to the ever increasing number of observations that become necessary to make the density estimates meaningful, as the number of sparsely populated bins increases as the number of dimensions increases \cite{wilkinson1995dataplot}.

	\section{Kernel Density Estimation}
		For density estimation, an oft-used alternative to histograms is kernel density estimation. This method places a `bump', the kernel, at each observation, and sums these to arrive at an estimate. See \autoref{fig:1:kdeIndividualKernels} for an example. 
		\begin{figure}[ht]
			\centering
			\includegraphics[width=0.5\textwidth]{./img/01_kde}
			\caption{A kernel density estimate of the one-dimensional dataset shown in red on the x-axis. For some observations the used normal kernels are shown in blue.}
			\label{fig:1:kdeIndividualKernels}
		\end{figure}

		In the univariate case this is formally defined as:
		\begin{equation} \label{eq:1:univariateKDE}
		\hat{f}(r) = (N \cdot h)^{-1} \sum_{i=1}^{N}  \left( K \left(\frac{r - r_i}{h} \right) \right).
		\end{equation}
		$K(\bullet)$ represents the kernel function. If we use a normal distribution as our kernel we could rewrite \autoref{eq:1:univariateKDE} to:
		\begin{align}\label{eq:1:univariateKDENormalKernel}
			\hat{f}(r) = \frac{1}{N} \sum_{i=1}^{N} K \left( r_i \right)
			&&
			K = \norm{1}{r}{h}
		\end{align}
		The smoothing parameter $h$ represents the kernel width, and data points are represented by $r$ \cite{wand1994kernel}. The parameter $h$ defines the width of the bump that is placed over observation $r$. It is clear from \autoref{eq:1:univariateKDE} that density estimate depends on the width and the shape of the kernel, in the same way that the density represented by a histogram is influenced by its bin size. 

			\begin{figure}[b]
				\centering
				\begin{subfigure}[b]{0.315\textwidth}
					\centering
					\includegraphics[width=\textwidth]{./img/01_kernels_uniform}
					\caption{Uniform $\left(\uni{1}{-2}{2}\right)$}
					\label{subfig:1:uniform}
				\end{subfigure}
				\begin{subfigure}[b]{0.315\textwidth}
					\centering
					\includegraphics[width=\textwidth]{./img/01_kernels_epan}
					\caption{Epanechnikov}
					\label{subfig:1:epan}
				\end{subfigure}										
				\begin{subfigure}[b]{0.315\textwidth}
					\centering
					\includegraphics[width=\textwidth]{./img/01_kernels_normal}
					\caption{Normal $\left(\norm{1}{0}{1}\right)$}
					\label{subfig:1:normal}
				\end{subfigure}		
				\caption{The (\subref{subfig:1:uniform}) uniform, (\subref{subfig:1:normal}) normal and (\subref{subfig:1:epan}) Epanechnikov kernel.}
				\label{fig:1:kernelShapes}
			\end{figure}

			\subsection{Kernel Shape} \label{ss:2:kernelShape}
				 A symmetric unimodal density is almost always chosen as kernel. This is not only because of the ease of interpretation but also due to compelling theoretical reasons \cite{wand1994kernel}. An in depth analysis of these grounds can be found in \textcite{cline1988admissibile}.

				The uniform kernel (\autoref{subfig:1:uniform}) is both symmetric and unimodal, but is hardly ever used,  since the resulting density estimation is piecewise constant. This means that the resulting density estimation is very jagged, such as the density estimate in \autoref{fig:1:piecewiseConstant}.

				\begin{figure}[t!]
					\centering
					\includegraphics[width=0.44\textwidth]{./img/01_pc}
					\caption{Kernel density estimation of the dataset used in \autoref{fig:1:kdeIndividualKernels}. The resulting estimate is a piece wise constant.}
					\label{fig:1:piecewiseConstant}
				\end{figure}

				The Epanechnikov kernel, shown in \autoref{subfig:1:epan}, is theoretically optimal because its asymptotic mean integrated squared error (AMISE) is zero. It has a finite base, making it faster than a normal kernel, all other things being equal \cite{wand1993comparison}. A downside of this kernel is that it are only differentiable between minus one and one \cite{silverman1986density}.

				Gaussian kernels (\autoref{subfig:1:normal}) are often used, but have a significant downside, they have infinite support. This means that for any point, all other points influence the final density estimation, as the density probability function of the Gaussian never reaches zero. Comparing \autoref{subfig:1:uniform} with \autoref{subfig:1:epan} we see that the Epanechnikov kernel has the value zero outside the range $[-1,1]$, while the normal kernel always stays above zero. This behaviour of the normal kernel is not desirable when one does not want to take into account all points in the dataset for the density estimation of one point. Another consequence of the infinite support is that the density estimation is computationally more expensive than when a finite support kernel is used.

				It can be shown that the shape of the kernel does not significantly influence the Mean Squared Error, since the errors hardly differ between the different kernels. We refer the reader to e.g. \textcite{silverman1986density} for an exhaustive treatment of performance differences between kernels. One should therefore select kernels based on other grounds than kernel efficiency since ``the `suboptimal' kernels are not suboptimal by very much"\cite{wand1994kernel}.

			\subsection{Kernel Width}
				% Importance of the kernel width
				The bandwidth of the kernel does have a significant impact on the performance, measured with the mean squared error, of the estimator. A kernel that is too small bases its averaging process on relatively few data points. This results in a very rough estimate of the underlying density and ignores variations across samples, as seen in \autoref{subfig:1:undersmoothing}. An estimation that suffers from this is called undersmoothed.

				A bandwidth that is too high may result in an oversmoothed estimate, such as in \autoref{subfig:1:oversmoothing}. Here, the bimodality of the density has been smoothed away. A correct kernel width shows the essential structure of the underlying density, without being overly noisy \cite{wand1994kernel}.

				\begin{figure}
					\centering
					\begin{subfigure}[b]{0.49\textwidth}
						\centering
						\includegraphics[width=0.9\textwidth]{./img/01_bandwith_undersmoothing}
						\caption{Undersmoothed estimate ($h = 0.05$)}
						\label{subfig:1:undersmoothing}
					\end{subfigure}
					\begin{subfigure}[b]{0.49\textwidth}
						\centering
						\includegraphics[width=0.9\textwidth]{./img/01_bandwith_oversmoothing}
						\caption{Oversmoothed estimate ($h = 0.75$)}
						\label{subfig:1:oversmoothing}
					\end{subfigure}		
					\caption{Kernel density estimates based on a sample of 1000 observations from $f(x) = \rfrac{3}{4} \cdot \mathcal{N}(0,1) + \rfrac{1}{4} \cdot \mathcal{N}(\rfrac{3}{2}, \rfrac{1}{9})$ using Gaussian kernels with a too small (\ref{subfig:1:undersmoothing}) and a too large (\ref{subfig:1:oversmoothing}) kernel width. The true density is shown in black, the estimated density in green. Adapted from \textcite{wand1994kernel}.}
					\label{fig:1:kernelWidth}
				\end{figure}

				% Selecting kernel width
				In many situations it is perfectly acceptable to choose a bandwidth by eye. One could for example generate several density estimations with different bandwidths, and choose the estimate that most accurately represents the data. However this method is time consuming, and only works when one has prior knowledge of the data's structure.

				For all other situations, the use of a bandwidth selector is preferred. This selector estimates a bandwidth based on the data, often without having any information about the underlying distribution. Another advantage of an estimated bandwidth, is that it can be adapted to the local data. These local kernel density estimators make it possible to use a different kernel at each point \cite{wand1994kernel}. Global estimators, by contrast, use the same smoothing parameter, and thus the same kernel for each observation. In the following we discuss both local and global kernel density estimators.

			\section{Kernel Density Estimators} \label{s:1:kdes}
				Broadly speaking, both basic and local kernel density estimators can be divided into two classes: classical and plug-in estimators.
				The classical methods use cross-validation to minimize a certain error with respect to the bandwidth. The plug-in methods define the bias of an estimate $\hat{f}$ as a function of the unknown density $f$. This bias is usually approximated through Taylor series expansion. ``A pilot estimate of $f$ is then `plugged in' to derive an estimate of the bias and hence an estimate of mean integrated squared error. The `optimal' $h$ minimizes this estimated measure of fit." \cite{loader1999bandwidth}

				% Silverman
				\subsection{Silverman Estimator}
					A simple example of a plug-in method is the Silverman rule-of-thumb estimator. This estimator assumes that the distribution underlying the data points is Gaussian. Under this assumption it can be proven, see \cite{silverman1986density}, that when the normal kernel is used the optimal bandwidth is:
					\begin{equation} \label{eq:1:silvermanEstimator}
						h_{silverman} = 1.06 \hat{\sigma}N^{-\frac{1}{5}}.
					\end{equation}	
					Where $\hat{\sigma}$ is the computed standard deviation of the sample, and $N$ denotes the number of samples \cite{silverman1986density}. The found value of $h_{silverman}$ can then be used in \eqref{eq:1:univariateKDE} in the univariate case. This estimator can be used in the multivariate case by filling a diagonal weight matrix with these widths. See \textcite{wand1994kernel} for a more exhaustive discussion of this application of the Silverman estimator.

				% Breiman
				\subsection{Breiman Estimator}
					The Breiman estimator is an example of a classical estimator. It uses the distance $\delta_{i,k}$ between $\vec{r}_i$ and its $k$th nearest neighbour to compute the local bandwidth. The kernel density estimate $\hat{f}(\vec{r})$ is defined as: 
						\begin{align}\label{eq:1:breimanEstimator}
							\hat{f}(\vec{r}) 
							= 
							\frac{1}{N} \sum_{i = 1}^{N} 							
								K\left( \vec{r_i} \right)
							&& 
							K
							=
							\norm{d}{\vec{r}}
							{
								\diag
								\left(
									{\left(
										\alpha \cdot \delta_{i,k}
									\right)}^{2}
								\right)
							}
						\end{align}
					Where $d$ represents the dimension of each data point. The parameters $k$ and $\alpha$ are determined by optimizing a goodness-of-fit criterion. 

					In a region with few data points the density is low, therefore $\delta_{i,k}$ is high and the kernel will be spread out. In high density regions $\delta_{i,k}$ will be low which results in smaller kernels \cite{breiman1977variable}. A downside of this approach is that it is computationally expensive. The estimator needs to run numerous times to find the optimal values for $\delta_{i,k}$ and $\alpha$ using least-squares cross-validation (LSCV). Another factor contributing to the computational complexity is the use of a kernel with infinite support.

				% MBE
				\subsection{Modified Breiman Estimator}
					\label{ss:1:mbe}
					The Modified Breiman Estimator introduced by \textcite{wilkinson1995dataplot} addresses these two causes of computational complexity. Firstly they replace the infinite support kernel with the finite support Epanechnikov kernel $\left( K_e(\bullet)\right)$. Furthermore they base the local bandwidth on a pilot density that is computed once, instead of using LSCV.

					This estimator starts by computing a pilot window $\sigma_{l}$ for each \mbox{dimension $l$}:
					\begin{equation}\label{eq:1:MBEsigmaL}
						\sigma_l = \frac{P_{80}(l) - P_{20}(l)}{\log N}, \; l = 1, \ldots d.
					\end{equation}
					$d$ denotes the dimensionality of the dataset and, $P_x(l)$ is the $x$th percentile of the data points in dimension $l$. The optimal pilot window width is defined as the minimum of the pilot windows of all dimensions:
					\begin{equation}\label{eq:1:MBEsigmaOpt}
					 	\sigma_{opt} = \min \{\sigma_1, \ldots \sigma_{d} \}.
					 \end{equation} 
					Using the optimal pilot window a pilot density $\hat{f}_{pilot}$ is defined:
					\begin{equation}\label{eq:1:MBEpilotDensity}
						\hat{f}_{pilot}(\vec{r}) = \frac{1}{N} \sum_{i = 1}^{N} \left( {\sigma_{opt}}^{-d} \cdot K_{e} \left( \frac{\vec{r} - \vec{r_i}}{\sigma_{opt}}\right) \right).
					\end{equation}
					This density function is used to compute the local bandwidth parameters $\gamma_i$ for each observation $\vec{r_i}$:
					\begin{equation}\label{eq:1:MBEgamma}
						\gamma_i = {\left(\frac{\hat{f}_{pilot}(\vec{r_i})}{\gm\left(\hat{f}_{pilot}(\vec{r}_1), \ldots, \hat{f}_{pilot}(\vec{r}_N)\right)}\right)}^{-\alpha}.
					\end{equation}
					$\alpha$ is the sensitivity parameter, which has been chosen to be $\rfrac{1}{d}$. The denominator is the geometric mean of all the pilot densities. The geometric mean is defined as:
					\begin{equation}\label{eq:1:geometricMean}
						\gm(\{a_1, \ldots a_n\}) = \sqrt[n]{\displaystyle\prod_{i=1}^{n} a_i}.
					\end{equation}
					The final estimate uses the local bandwidth parameter to scale the optimal pilot window size:
					\begin{equation}\label{eq:1:MBEdensityEstimate}
						\hat{f}(\vec{r}) = \frac{1}{N} \sum_{i = 1}^{N} \left( \left( {\sigma_{opt} \cdot \gamma_i} \right)^{-d} \cdot K_{e} \left( \frac{\vec{r} - \vec{r_i}}{\sigma_{opt} \cdot \gamma_i}\right) \right).
					\end{equation}

				The three presented density estimators: the Silverman, Breiman and Modified Breiman estimator, are only a selection of the abundance of methods described in literature. There is a lot of discussion on what class of estimator works best. \textcite{walt2013kernel} found that the rule-of-thumb estimators show consistent accurate performance, whereas the cross-validation estimators did not perform well in datasets with high dimensionality. \textcite{loader1999bandwidth} however argues that plug-in methods are subject to the same criticism as the classical approaches. For a more exhaustive comparison of different (kernel) density estimators see \textcite{jones1997comparison}, \textcite{wand1993comparison} or \textcite{rudemo1982empirical}, among others.

			\noindent In this work we propose a kernel density estimator of which not only the smoothing parameter, but also the shape of the kernel depends on the data. The kernel is therefore sensitive to the orientation of the data. We expect that an estimator with a shape-adapted kernel outperforms the same estimator with a kernel that is only smoothed. We have opted to use the Modified Breiman Estimator, based on the results found by \textcite{ferdosi2011comparison}. Therefore we have named our method the Adapted Modified Breiman Estimator (AMBE).

			In \cref{sec:method} we consider the algorithm to adapt the shape of the kernel, and discuss the steps it takes in detail. \Cref{sec:results} presents and discusses the differences in performance between smoothed shape-adapted kernels and smoothed kernels when used with the \mbe. The last chapter offers an conclusion and presents ideas for further research.



		
