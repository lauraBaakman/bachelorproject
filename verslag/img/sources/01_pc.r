# Remove al existing variables
rm(list = ls())

plotKernel <- function(mean){
	x   <- seq(mean - r, mean + r, length.out = n)
	y1   <- dnorm(x, mean, sd)
	lines(x,y1, 
		col=myGreen,
		ylim = c(0, 0.035), 
		xlim = c(0, 100)
	)	
	lines(c(mean, mean), c(0, max(y1)));
}

# Define colours
myGreen <- rgb(10, 103, 58, max= 255);
myBlue <- rgb(43, 56, 143, max= 255);
myRed <- rgb(190, 30, 45, max= 255);

# Load libraries
library(KernSmooth)

# Set the directory
setwd("/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/sources/");
 
# Use the old faithfull data
attach(faithful)

# Modifiy data
waiting = waiting - 30;

# # Select points to be plotted
# ind = c(3, 68, 136, 204, 271);
# plotWaiting = waiting[ind];

# Estimate the density 
fhat <- bkde(x=waiting, kernel='box')

# Scale density
fhat$y = fhat$y * 30;

pdf("/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/01_pc.pdf");
par(lwd = 1.5,
	mar = c(2, 2.2, 0.1, 0.1),
	xaxt = 'n',
	yaxt = 'n',
	ann = FALSE
)

plot(fhat, col = myBlue, type='l')
mtext('x', side=1, line=1, cex=1.5, las=1)
mtext('Density', side=2, line=1, cex=1.5)

dev.off()




