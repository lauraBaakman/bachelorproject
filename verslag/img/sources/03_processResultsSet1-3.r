plotChar = 19;

set1 <- function(result){
	lim = c(10, 41)
	A = getRangeByLocation(result, lim, lim, lim);
	Abar = getRangeByLocation(result, lim, lim, lim, TRUE);
	setNumber = 1;
	val = c(result$AMBE.computedD, result$MBE.computedD, result$trueD);
	axes = c(min(val), max(val))


	# AMBE
	pdf('../03_AMBE_result_N2400_01.pdf', useDingbats=FALSE);
	title = paste('AMBE', " - dataset ", setNumber, sep="");
	mse = signif(mean(result$AMBE.MSE),3)
	plot(A$trueD, A$AMBE.computedD,
		ylim = axes,
		xlab = "true density",
		ylab = "esitmated density",
		# asp=1,
		main = title,
		# sub = filename,
		col = myRed,
		pch = plotChar
	)

	points(Abar$trueD, Abar$AMBE.computedD,
		col = myBlue,
		pch = plotChar
	)

	lines(result$trueD, result$trueD, cex=0.5, pch = 1)
	mtext(paste("(MSE = ", mse, ")"), side = 3, line = 0.3, cex = 1.5)
	dev.off()

	# MBE
	pdf('../03_MBE_result_N2400_01.pdf', useDingbats=FALSE);
	title = paste('MBE', " - dataset ", setNumber, sep="");
	mse = signif(mean(result$MBE.MSE),3)
	plot(A$trueD, A$MBE.computedD,
		ylim = axes,
		xlab = "true density",
		ylab = "esitmated density",
		# asp=1,
		main = title,
		# sub = filename,
		col = myRed,
		pch = plotChar
	)

	points(Abar$trueD, Abar$MBE.computedD,
		col = myBlue,
		pch = plotChar
	)

	lines(result$trueD, result$trueD, cex=0.5, pch = 1)
	mtext(paste("(MSE = ", mse, ")"), side = 3, line = 0.3, cex = 1.5)
	dev.off();
}

set2 <- function(result, export = TRUE){
 	limA = c(4,20.5)
 	A = getRangeByLocation(result, limA, limA, limA)
	
 	limB = c(20.7, 44.8)
 	B = getRangeByLocation(result, limB, limB, limB)

 	C = getRangeByLocation(result, limA, limA, limA, TRUE);
 	C = getRangeByLocation(C, limB, limB, limB, TRUE);

	setNumber = 2;
	val = c(result$AMBE.computedD, result$MBE.computedD, result$trueD);
	axes = c(min(val), max(val))


	# AMBE
	if(export){
		pdf('../03_AMBE_result_N2400_02.pdf', useDingbats=FALSE);
	} else {
		par(mfrow = c(1,2))
	}
	title = paste('AMBE', " - dataset ", setNumber, sep="");
	mse = signif(mean(result$AMBE.MSE),3)
	plot(A$trueD, A$AMBE.computedD,
		ylim = axes,
		xlab = "true density",
		ylab = "esitmated density",
		# asp=1,
		main = title,
		# sub = filename,
		col = myRed,
		pch = plotChar
	)

	points(B$trueD, B$AMBE.computedD,
		col = myBlue,
		pch = plotChar
	)
	points(C$trueD, C$AMBE.computedD,
		col = myGreen,
		pch = plotChar
	)	

	lines(result$trueD, result$trueD, cex=0.5, pch = 1)
	mtext(paste("(MSE = ", mse, ")"), side = 3, line = 0.3, cex = 1.5)
	if(export){
		dev.off()
	}

	# MBE
	if(export){
		pdf('../03_MBE_result_N2400_02.pdf', useDingbats=FALSE);
	}
	title = paste('MBE', " - dataset ", setNumber, sep="");
	mse = signif(mean(result$MBE.MSE),3)
	plot(A$trueD, A$MBE.computedD,
		ylim = axes,
		xlab = "true density",
		ylab = "esitmated density",
		# asp=1,
		main = title,
		# sub = filename,
		col = myRed,
		pch = plotChar
	)

	points(B$trueD, B$MBE.computedD,
		col = myBlue,
		pch = plotChar
	)

	points(C$trueD, C$MBE.computedD,
		col = myGreen,
		pch = plotChar
	)	

	lines(result$trueD, result$trueD, cex=0.5, pch = 1)
	mtext(paste("(MSE = ", mse, ")"), side = 3, line = 0.3, cex = 1.5)
	if(export){
		dev.off()
	}
}

set3 <- function(result, export = TRUE){
	lim = c(12, 18);
 	A = getRangeByLocation(result, lim, lim, lim)
 	B = getRangeByLocation(result, lim, lim, lim, TRUE)


	setNumber = 3;
	val = c(result$AMBE.computedD, result$MBE.computedD, result$trueD);
	axes = c(min(val), max(val))


	# AMBE
	if(export){
		pdf('../03_AMBE_result_N2400_03.pdf', useDingbats=FALSE);
	} else {
		par(mfrow = c(1,2))
	}
	title = paste('AMBE', " - dataset ", setNumber, sep="");
	mse = signif(mean(result$AMBE.MSE),3)
	plot(A$trueD, A$AMBE.computedD,
		ylim = axes,
		xlab = "true density",
		ylab = "esitmated density",
		# asp=1,
		main = title,
		# sub = filename,
		col = myRed,
		pch = plotChar
	)

	points(B$trueD, B$AMBE.computedD,
		col = myBlue,
		pch = plotChar
	)

	lines(result$trueD, result$trueD, cex=0.5, pch = 1)
	mtext(paste("(MSE = ", mse, ")"), side = 3, line = 0.3, cex = 1.5)
	if(export){
		dev.off()
	}

	# MBE
	if(export){
		pdf('../03_MBE_result_N2400_03.pdf', useDingbats=FALSE);
	}
	title = paste('MBE', " - dataset ", setNumber, sep="");
	mse = signif(mean(result$MBE.MSE),3)
	plot(A$trueD, A$MBE.computedD,
		ylim = axes,
		xlab = "true density",
		ylab = "esitmated density",
		# asp=1,
		main = title,
		# sub = filename,
		col = myRed,
		pch = plotChar
	)

	points(B$trueD, B$MBE.computedD,
		col = myBlue,
		pch = plotChar
	)

	lines(result$trueD, result$trueD, cex=0.5, pch = 1)
	mtext(paste("(MSE = ", mse, ")"), side = 3, line = 0.3, cex = 1.5)
	if(export){
		dev.off()
	}
}