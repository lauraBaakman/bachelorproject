# Remove al existing variables
rm(list = ls())
printf <- function(...) cat(sprintf(...))

# Set the directory
setwd("/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/sources/");

parseToDF <- function(data){
	frame <- data.frame(MSE = data[2][[1]], computedD = data[3][[1]], trueD = data[4][[1]])
	return(frame);
}

handleFile <- function(filename, setNumber){
	matFile <- readMat(filename);

	# Convert to R objects
	AMBE <- parseToDF(matFile$AMBE);
	MBE <- parseToDF(matFile$MBE);
	AMBE.MSE = signif(matFile$AMBE[1][[1]][1,1],3)
	MBE.MSE = signif(matFile$MBE[1][[1]][1,1],3)

	# Select limits for the axes
	val = c(AMBE$computedD, MBE$computedD, AMBE$trueD);
	axes = c(min(val), max(val))
	rm(val);

	# Plot
	generatePlots(filename, setNumber, 'AMBE', AMBE, AMBE.MSE, axes, prop[setNumber,]);
	generatePlots(filename, setNumber, 'MBE', MBE, MBE.MSE, axes, prop[setNumber,]);
	return(c(AMBE.MSE, MBE.MSE));
}

generatePlots <- function(filename, setNumber, algoritme, data, mse, axes, prop){
	# Output filename
	outputFilename = paste('../03_', algoritme, '_result_N', N, '_0', setNumber, '.pdf', sep='');

	#Title 
	title = paste(algoritme, " - dataset ", setNumber, sep="");

	#Define colours
	cols =c(rep(myRed, prop[1] * N), 
			rep(myBlue, prop[2] * N), 
			rep(myGreen, prop[3] * N));

	# Create image files
	pdf(outputFilename)
	# dev.new();

	plot(data$trueD, data$computedD,
		# xlim = axes,
		ylim = axes,
		xlab = "true density",
		ylab = "esitmated density",
		# asp=1,
		main = title,
		# sub = filename,
		col = cols
	)

	lines(data$trueD, data$trueD, cex=0.5, pch = 1)
	mtext(paste("(MSE = ", mse, ")"), side = 3, line = 0.3, cex = 1.5)
	grid(5, 5)

	dev.off()
}

# Set the size of the dataset to be processed
N = 2400

# Split the datasets in their subsets
prop = data.frame(
	rbind(
		c(2/3, 1/3, 0),  	
		c(1/3, 1/3, 1/3),	
		c(1/3, 1/3, 1/3),		
		c(1/5, 4/5, 0),	
		c(2/5, 2/5, 1/5)	
	),
	row.names=seq(1, 5, 1)
);
names(prop) = c('x', 'y', 'z')

# Define colours
myGreen <- rgb(10, 103, 58, max= 255);
myBlue 	<- rgb(43, 56, 143, max= 255);
myRed 	<- rgb(190, 30, 45, max= 255);

# Load libraries
library(R.matlab);
library(scatterplot3d)
library(stringr)


# Create a list of files
files = list.files(path = "./03_results", pattern = paste('*', N, '*', sep=''), all.files = FALSE,
           full.names = TRUE, recursive = FALSE,
           ignore.case = FALSE, include.dirs = FALSE, no.. = FALSE)

# par(mfrow= c(4, 2))
par(mfrow= c(1, 1))

results = data.frame();
for (i in seq(1, 3)){
	file = files[i];
	res = handleFile(file, i);
	results = rbind(results, cbind(file, res[1], res[2]));
}
names(results) <- c("file", "AMBE", "MBE")
results$AMBE = as.numeric(as.character(results$AMBE))
results$MBE = as.numeric(as.character(results$MBE))
results$diff = results$AMBE = results$MBE