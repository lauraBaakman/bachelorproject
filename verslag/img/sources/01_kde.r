# Remove al existing variables
rm(list = ls())

plotKernel <- function(mean){
	x   <- seq(mean - r, mean + r, length.out = n)
	y1   <- dnorm(x, mean, sd)
	lines(x,y1, 
		col=myGreen,
		ylim = c(0, 0.035), 
		xlim = c(0, 100)
	)	
	lines(c(mean, mean), c(0, max(y1)), col=myRed, cex = 2);
}

# Define colours
myGreen <- rgb(10, 103, 58, max= 255);
myBlue <- rgb(43, 56, 143, max= 255);
myRed <- rgb(190, 30, 45, max= 255);

# Load libraries
library(KernSmooth)

# Set the directory
setwd("/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/sources/");
 
 # Use the old faithfull data
 attach(faithful)

# Modifiy data
waiting = waiting - 30;

# Select points to be plotted
ind = c(3, 68, 136, 204, 271);
plotCross = waiting[ind];

indPlotBar = seq(1, length(waiting), 1);
indPlotBar = indPlotBar[ !indPlotBar %in% ind];
plotBar = waiting[indPlotBar];

# Estimate the density 
fhat <- bkde(x=waiting)

# Scale density
fhat$y = fhat$y * 30;

pdf("/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/01_kde.pdf");
par(lwd = 1.5,
	mar = c(2, 2.2, 0.1, 0.1),
	xaxt = 'n',
	yaxt = 'n',
	ann = FALSE
)

# Plot the crosses 
plot(plotCross, rep.int(0,length(plotCross)), 
		ylim = c(0, ceiling(max(fhat$y))), 
		xlim = c(0, ceiling(max(waiting))),
		ylab="Density",
		xlab="Data points",
		pch= 4,
		col = myRed,
		lwd = 3
);

#Plot all other dataPoints
points(plotBar, rep.int(0, length(plotBar)),
		pch='|',
		col = myRed);

mtext('Data points', side=1, line=1, cex=1.5, las=1)
mtext('Density', side=2, line=1, cex=1.5)

# Density
lines(fhat, col = myBlue)

# Define the kernels
sd = 1.5;
n = 1000;
r = 100;

# Plot the kernels
for (x in seq(1, length(ind))){
	plotKernel(plotCross[x])
}
dev.off()




