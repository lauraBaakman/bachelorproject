# Reset everything
rm(list = ls());

resetPar <- function() {
    dev.new()
    op <- par(no.readonly = TRUE)
    dev.off()
    return(op)
}

par(resetPar());

# Set the directory
setwd("/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/sources/");

#Library
library(stats)

plotkernel <- function(x,y){
	par(lwd = 2,
		mar = c(3.75, 3.85, 0, 0) + 0.1,
		cex = 1.5
		# xaxt = 'n',
		# yaxt = 'n'
	)
	plot(x, y, 
			xlim = c(-3, 3), 
			ylim = c(0, 0.8),
			ylab="Density",
			xlab="x",
			type='l'
	);	
}
par(
	mfrow = c(1,1))

epanechnikov <- function(x){
	return(3/4 * (1 - x * x));
}

x <- seq(-3, 3, 0.001)

# Normal distribution
pdf('/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/01_kernels_normal.pdf')
plotkernel(x, dnorm(x,mean=0, sd=1));
dev.off();

# Uniform distribution 
pdf('/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/01_kernels_uniform.pdf')
plotkernel(x, dunif(x, min=-2, max=2))
dev.off();

# Epanechnikov distribution
pdf('/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/01_kernels_epan.pdf')
y <- c(rep(0, 2000), epanechnikov(seq(-1,1,0.001)), rep(0,2000));
plotkernel(x, y);
dev.off();