# Remove al existing variables
rm(list = ls())

resetPar <- function() {
    dev.new()
    op <- par(no.readonly = TRUE)
    dev.off()
    op
}

par(resetPar());

# Define colours
myGreen <- rgb(10, 103, 58, max= 255);
myBlue <- rgb(43, 56, 143, max= 255);
myRed <- rgb(190, 30, 45, max= 255);

# Set the directory
setwd("/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/sources/");

#The number of samples from the mixture distribution
N = 1000              

#Sample N random uniforms U
U =runif(N)

#Variable to store the samples from the mixture distribution                                             
rand.samples = rep(NA,N)

#Sampling from the mixture
for(i in 1:N){
    if(U[i]<.75){
        rand.samples[i] = rnorm(1,0,1)
    }else{
        rand.samples[i] = rnorm(1,3/2,1/9)
    }
}

# Get true density
x = seq(-5,5,.01)
x + 40;
truth = .75*dnorm(x,0,1) + .25*dnorm(x,3/2,1/9)

ylimits = c(0, max(truth));

histogram = function(bin, i){
	pdf(paste('/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/01_hist_0', i, '.pdf', sep=''))
	par(ann=FALSE,
		xaxt ='n',
		yaxt ='n',
		mar = c(0, 0, 0, 0))
	plot(x, truth, ylim = ylimits, type='l', col=myRed, lwd=5)
	h = hist(rand.samples, bin, freq=FALSE, add=TRUE);
	dev.off()
}

histogram(2, 1)
histogram(10, 2)
histogram(79, 3)


# pdf('/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/01_hist_01.pdf')
# par(ann=FALSE,
# 	xaxt ='n',
# 	yaxt ='n',
# 	mar = c(0, 0, 0, 0))
# plot(x, truth, ylim = ylimits, type='l', col=myRed)
# hist(rand.samples, 1, freq=FALSE, add=TRUE);
# dev.off()

# pdf('/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/01_hist_02.pdf')
# par(ann=FALSE,
# 	xaxt ='n',
# 	yaxt ='n',
# 	mar = c(0, 0, 0, 0))
# plot(x, truth, ylim = ylimits, type='l', col=myRed)
# hist(rand.samples, 5, freq=FALSE, add=TRUE);
# dev.off()

# pdf('/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/01_hist_03.pdf')
# par(ann=FALSE,
# 	xaxt ='n',
# 	yaxt ='n',
# 	mar = c(0, 0, 0, 0))
# plot(x, truth, ylim = ylimits, type='l', col=myRed)
# hist(rand.samples, 40, freq=FALSE, add=TRUE);
# dev.off()