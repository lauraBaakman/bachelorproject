# Remove al existing variables
rm(list = ls())

# Load libraries
library(mnormt);

#Define colours
myGreen <- rgb(10, 103, 58, max= 255);
myBlue <- rgb(43, 56, 143, max= 255);
myRed <- rgb(190, 30, 45, max= 255);

# Set the directory
setwd("/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/sources/");

# Number of data points
n = 20000;

# Generate data
mu <- c(2, 3);
var <- matrix(c(1, 1.5, 1.5, 3), 2, 2);
data <- rmnorm(n, mu, var);

# Covariance matrix
covm <- cov(data);

# Eigen vectors and values
eigens <- eigen(covm);
evs <- sqrt(eigens$values);
evecs <- eigens$vectors;

# Something
a <- evs[1];
b <- evs[2];
x0 <- 0;
y0 <- 0;
alpha <- atan(evecs[ , 1][2] / evecs[ , 1][1]);
theta <- seq(0, 2 * pi, length=(1000));

# Mean
x = mean(data[,1])
y = mean(data[,2])
ctr = c(x, y);

# Genereer data voor contour plot
factor = 15;
X = seq(x - factor, x + factor, length.out = 1000);
Y = seq(y - factor, y + factor, length.out = 1000);
kernel = function(q, t) {
	dmnorm(cbind(q,t), mean=c(x, y), covm)
}
Z = outer(X, Y, kernel);

# Genereer ellipse
# RR <- chol(covm);
# angles <- seq(0, 2*pi, length.out=200);
# ell    <- 1 * cbind(cos(angles), sin(angles)) %*% RR
# ellCtr <- sweep(ell, 2, colMeans(data), "+")
eigScl  <- evecs %*% diag(evs)  # scale eigenvectors
angles <- seq(0, 2*pi, length.out=200)
xMat    <- rbind(ctr[1] + eigScl[1, ], ctr[1] - eigScl[1, ])
yMat    <- rbind(ctr[2] + eigScl[2, ], ctr[2] - eigScl[2, ])
ellBase <- cbind(sqrt(evs[1]^2)*cos(angles), sqrt(evs[2]^2)*sin(angles)) # normal ellipse
ellRot  <- evecs %*% t(ellBase) 

Plot
pdf('/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/02_mathBasis_EigenValues.pdf')

par(
	ann=FALSE,
	xaxt = 'n',
	yaxt = 'n',
	mar = c(0, 0, 0, 0)
	)

# De punten
plot(data[,1], data[,2], pch='.', col="black", type='p');

# Contour plot
contour(X, Y, Z, nlevels=10, col=myRed, labels="", add=TRUE,)

# De ellips
lines((ellRot+ctr)[1, ], (ellRot+ctr)[2, ], lwd=1.5, col=myBlue)

# De eigenvectoren
matlines(xMat, yMat, lty=1, lwd=3, col=myBlue)

# Het midden
points(x, y, col=myBlue, pch=19, cex=1.5)

dev.off()

