# Remove al existing variables
# rm(list = ls())
printf <- function(...) cat(sprintf(...))

# Set the size of the dataset to be processed
N = 2400

# Split the datasets in their subsets
# Set the directory
setwd("/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/sources/");


# Define colours
myGreen <- rgb(10, 103, 58, max= 255);
myBlue 	<- rgb(43, 56, 143, max= 255);
myRed 	<- rgb(190, 30, 45, max= 255);

# Load libraries
library(R.matlab);
library(scatterplot3d)
library(stringr)

parseToDF <- function(data){
	frame <- data.frame(MSE = data[2][[1]], computedD = data[3][[1]], trueD = data[4][[1]])
	return(frame);
}

handleFile <- function(filename, setNumber){
	matFile <- readMat(filename);

	# Convert to R objects
	AMBE <- parseToDF(matFile$AMBE);
	MBE <- parseToDF(matFile$MBE);
	AMBE.MSE = signif(matFile$AMBE[1][[1]][1,1],3)
	MBE.MSE = signif(matFile$MBE[1][[1]][1,1],3)
	points = matFile$data;

	# Select limits for the axes
	val = c(AMBE$computedD, MBE$computedD, AMBE$trueD);
	axes = c(min(val), max(val))
	rm(val);

	# Plot
	# pdf("03_set5_r.pdf", height = 3/8 * 10, useDingbats=FALSE)
	generatePlots("AMBE - dataset 5", AMBE, axes);
	generatePlots("MBE - dataset 5", MBE, axes);
	# dev.off()
}

generatePlots <- function(title, data, axes){
	axes = c(0,0.001)

	# Red
	A = data[which(data$trueD == unique(data$trueD)[1]),];

	# Blue
	B = data[which(data$trueD == unique(data$trueD)[2]),];

	# Green
	C = data[which(data$trueD == unique(data$trueD)[3]),];

	plot(A$computedD, rep(0.03, length(A$computedD)),
		xlab = "estimated density",
		ylab = " ",
		yaxt = "n",
		ylim = c(-0.11, 0.05),
		xlim = axes,
		main = title,
		col = myRed,
		pch = 3
	)
	mtext(paste("(MSE = ", signif(mean(data$MSE),3), ")"), side = 3, line = 0.3, cex = 1.5)

	points(B$computedD, rep(-0.03, length(B$computedD)),
		col = myBlue,
		pch = 3
	)

	points(C$computedD, rep(-0.09, length(C$computedD)),
		col = myGreen,
		pch = 3
	)

	points(A$trueD[1], 0, col = myRed, pch = 21, bg = myRed)
	points(B$trueD[1], 0, col = myBlue, pch = 21, bg = myBlue)
	points(C$trueD[1], 0, col = myGreen, pch = 21, bg = myGreen)
}

filename = "./03_results/03_results_N2400_set_05.mat"
par(
	mfrow = c(2,1))

handleFile(filename, 5);

