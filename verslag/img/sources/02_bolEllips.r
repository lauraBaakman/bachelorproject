# Remove al existing variables
rm(list = ls())

# Define colours
myGreen <- rgb(10, 103, 58, max= 255);
myBlue <- rgb(43, 56, 143, max= 255);
myRed <- rgb(190, 30, 45, max= 255);

# Load libraries
library(KernSmooth)
library(scatterplot3d)
library(mnormt);

# Set the directory
setwd("/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/sources/");

#The number of samples
n = 30000  

# The dimension
dim <- 3;            

## Sphere
sphere.mu = rep(15, dim);
sphere.var = diag(15, 3, 3);
sphere.data <- rmnorm(n, sphere.mu, sphere.var);

# Ellipsoid
ell.mu = c(40, 20, 40)
ell.var = diag(c(5, 50, 5), 3, 3);
ell.data <- rmnorm(n, ell.mu, ell.var);

data = data.frame(x = c(sphere.data[,1], ell.data[,1]), y = c(sphere.data[,2], ell.data[,2]), z = c(sphere.data[,3], ell.data[,2]))


pdf('/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/01_bolEllips_data.pdf')
	par(pch=21,
		mar = c(0, 0, 0, 0),
		cex.lab = 2,
		pty="s"
	);
	ticks = c(0,50,100);
	scatterplot3d(data$x, data$y, data$z, 
		pch='.', 
		color=c(rep(myRed, n), rep(myBlue, n)),
		xlab='',ylab='',zlab='',
		label.tick.marks = FALSE);
	mtext('x', side=1, line=0, cex=1.5, adj=0.3, las=1)
	text(7.5,0, 'y', cex=1.5)
	mtext('z', side=2, line=0, cex=1.5, las=1)
dev.off();

pdf('/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/01_bolEllips_density.pdf')
	par(pch=21,
		mar = c(2, 2.2, 0.1, 0.1),
		cex.lab = 2,
		pty="s"
	);
	x = seq(0,50, length.out = 1000)
	y = seq(-10,50, length.out = 1000)
	z = seq(-10,50, length.out = 1000)
	density = 0.5 * dmnorm(cbind(x, y, z),sphere.mu, sphere.var) + 
			  0.5 * dmnorm(cbind(x, y, z), ell.mu, ell.var);
	plot(
		seq(0,10, length.out=1000),
		density,
		xlab="",
		ylab="",
		type='l',
		lwd = 2,
		xaxt = 'n',
		yaxt = 'n'
	);
	mtext('x', side=1, line=1, cex=1.5, las=1)
	mtext('Density', side=2, line=1, cex=1.5)
dev.off();
