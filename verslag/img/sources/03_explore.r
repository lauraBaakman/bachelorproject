# Set the directory
setwd("/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/sources/");

# Remove al existing variables
rm(list = ls())
printf <- function(...) cat(sprintf(...))

#Libraries
library(R.matlab);
library(scatterplot3d)

# Define colours
myGreen <- rgb(10, 103, 58, max= 255);
myBlue <- rgb(43, 56, 143, max= 255);
myRed <- rgb(190, 30, 45, max= 255);

N = 2400

resultsDir = "./03_results"
setsDir = "./02_datasets"

prop = data.frame(
	rbind(
		c(2/3, 1/3, 0),  	
		c(1/3, 1/3, 1/3),	
		c(1/3, 1/3, 1/3),		
		c(1/5, 4/5, 0),	
		c(2/5, 2/5, 1/5)	
	),
	row.names=seq(1, 5, 1)
);
names(prop) = c('x', 'y', 'z')

showResults <- function(setNumber){
	# Select limits for the axes
	val = c(results[[setNumber]]$AMBE.computedD, results[[setNumber]]$MBE.computedD, results[[setNumber]]$trueD);
	axes = c(min(val), max(val))
	rm(val);

	par(
		mfrow = c(3,2)
	)

	# Plot
	generatePlots(setNumber, 'AMBE', results[[setNumber]]$AMBE.computedD, results[[setNumber]]$trueD, AMBE.MSES[[setNumber]], axes, prop[setNumber,]);
	generatePlots(setNumber, 'MBE', results[[setNumber]]$MBE.computedD, results[[setNumber]]$trueD, MBE.MSES[[setNumber]], axes, prop[setNumber,]);
}

generatePlots <- function(setNumber, algoritme, computedD, trueD, mse, axes, prop){
	#Title 
	title = paste(algoritme, " - dataset ", setNumber, sep="");

	#Define colours
	cols =c(rep(myRed, prop[1] * N), 
			rep(myBlue, prop[2] * N), 
			rep(myGreen, prop[3] * N));

	# Create image files

	plot(trueD, computedD,
		xlim = axes,
		ylim = axes,
		xlab = "true density",
		ylab = "esitmated density",
		# asp=1,
		main = title,
		col = cols
	)

	lines(trueD, trueD, cex=0.5)
	mtext(paste("(MSE = ", mse, ")"), side = 3, line = 0.3, cex = 1.5)
	grid(5, 5)
}

parseToDF <- function(data){
	frame <- data.frame(MSE = data[2][[1]], computedD = data[3][[1]], trueD = data[4][[1]])
	return(frame);
}

readRun <- function(filename, index){
	# Read the file
	matFile <- readMat(filename);	

	# Parse
	AMBE.temp <- parseToDF(matFile$AMBE);
	MBE.temp <- parseToDF(matFile$MBE);
	AMBE.MSE.temp = signif(matFile$AMBE[1][[1]][1,1],3)
	MBE.MSE.temp = signif(matFile$MBE[1][[1]][1,1],3)
	x = matFile$data[,1];
	y = matFile$data[,2];
	z = matFile$data[,3];
	fa = matFile$fa;


	temp = data.frame(
			AMBE.MSE = AMBE.temp$MSE, AMBE.computedD = AMBE.temp$computedD,
			MBE.MSE = MBE.temp$MSE, MBE.computedD = MBE.temp$computedD, 
			trueD = MBE.temp$trueD, x = x, y = y, z = z, fa = fa);

	results[[index]] <<- temp;
}

readResults <- function(N){
	files = list.files(path = resultsDir, pattern = paste('03_results_N', N, '.*', sep=''), full.names = TRUE)
	for(i in 1:length(files)){
		readRun(files[i], i);
	}	
}

tableF <- function(setIdentifier, set){
	if(is.numeric(setIdentifier)){
		# Complete dataset
		printf("$%d$&	-&", setIdentifier)
	} else {
		# Subset
		printf("~&	%s&", setIdentifier)
	}
	printf("\\num{%e}&\\num{%e}&	%d& 	\\num{%e}& 	\\num{%e}\\\\", mean(set$AMBE.MSE), mean(set$MBE.MSE), length(set$AMBE.MSE), mean(set$fa), IQR(set$fa))
}

# readSets <- function(N){
# 	files = list.files(path = setsDir, pattern = paste('N', N, '_.*', sep = ''), full.names = TRUE)
# 	for(i in 1:length(files)){
# 		filename = files[i];

# 		#Read the file
# 		matFile <- readMat(filename);

# 		#Create dataframe
# 		frame <- data.frame(fa = matFile$fa, x = matFile$data[,1], y = matFile$data[,2], z = matFile$data[,3], trueDSet = matFile$tv)

# 		#Add fractional isotropy
# 		data[[i]] <<- frame;
# 		results[[i]] <<- cbind(results[[i]], frame);
# 	}
# }

getRangeByLocation <- function(dataFrame, xRa = c(0,50), yRa = c(0,50), zRa = c(0,50), inverse = FALSE){
	if(inverse){
		subset(dataFrame, 
			x < xRa[1] | x> xRa[2] |
			y < yRa[1] | y> yRa[2] |
			z < zRa[1] | z> zRa[2])
	} else {
		subset(dataFrame, 
			x >= xRa[1] & x<= xRa[2] &
			y >= yRa[1] & y<= yRa[2] &
			z >= zRa[1] & z<= zRa[2])
	}
}

getRangeByProp <- function(dataFrame, lower = 0, upper = 1){
	N = dim(dataFrame)[1];
	low = ceiling(N * lower);
	up = ceiling(N * upper);
	return(dataFrame[seq(low,up), ]);
}

results <- list();

readResults(N);
# readSets(N);

rm(resultsDir, setsDir)