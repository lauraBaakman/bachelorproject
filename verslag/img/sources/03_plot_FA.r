# Remove al existing variables
# rm(list = ls())

# Define colours
myGreen <- rgb(10, 103, 58, max= 255);
myBlue <- rgb(43, 56, 143, max= 255);
myRed <- rgb(190, 30, 45, max= 255);

# Load libraries
library(R.matlab);
library(scatterplot3d)

# Set the directory
setwd("/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/sources/");

handleDataset <- function(fileName, location="", n1=0, n2=0, n3=0){
	# Read mat file, NB save in matlab with -V6
	matFile <- readMat(paste(location, paste(fileName, "mat", sep="."), sep=""));
	N <- matFile$N;
	data <- data.frame(x = matFile$data[,1], y = matFile$data[,2], z = matFile$data[,3], fa = matFile$fa);

	# Plot
	pdf(paste("/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/", paste(fileName, "_fa.pdf", sep=""), sep = ""))

	par(pch=21,
		mar = c(0, 0, 0, 0),
		cex.lab = 2
	);

	numCol = 10;

	colours <- heat.colors(numCol);
	col <- colours[cut(data$fa,numCol)]

	scatterplot3d(data$x, data$y, data$z, 
		pch='.', 
		color= col,
		# xlim = c(min(data$x), max(data$x)),
		xlab='x',ylab='y',zlab='z',
		label.tick.marks = FALSE,
		pty="s",
		mar= c(1,1,0,0));
	dev.off()	
}


# handleDataset('02_datasets_01',"./02_datasets/" , 2/3, 1/3);
# handleDataset('02_datasets_02',"./02_datasets/" , 1/3, 1/3, 1/3);
# handleDataset('02_datasets_03',"./02_datasets/" , 2/5, 2/5, 1/5);
# handleDataset('02_datasets_04',"./02_datasets/" , 1/3, 1/3, 1/3);
# handleDataset('02_datasets_05',"./02_datasets/" , 1/3, 1/3, 1/3);
handleDataset('02_datasets_06',"./02_datasets/" , 1/3, 1/3, 1/3);


