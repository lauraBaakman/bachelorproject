# Remove al existing variables
rm(list = ls())
printf <- function(...) cat(sprintf(...))

# Set the size of the dataset to be processed
N = 2400

# Split the datasets in their subsets
prop = c(1/5, 4/5, 0);

# Set the directory
setwd("/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/sources/");


# Define colours
myGreen <- rgb(10, 103, 58, max= 255);
myBlue 	<- rgb(43, 56, 143, max= 255);
myRed 	<- rgb(190, 30, 45, max= 255);

# Load libraries
library(R.matlab);
library(scatterplot3d)
library(stringr)

parseToDF <- function(data){
	frame <- data.frame(MSE = data[2][[1]], computedD = data[3][[1]], trueD = data[4][[1]])
	return(frame);
}

handleFile <- function(filename, setNumber){
	matFile <- readMat(filename);

	# Convert to R objects
	AMBE <- parseToDF(matFile$AMBE);
	MBE <- parseToDF(matFile$MBE);
	AMBE.MSE = signif(matFile$AMBE[1][[1]][1,1],3)
	MBE.MSE = signif(matFile$MBE[1][[1]][1,1],3)
	points = matFile$data;

	# Select limits for the axes
	val = c(AMBE$computedD, MBE$computedD, AMBE$trueD);
	axes = c(min(val), max(val))
	rm(val);

	# Plot
	pdf("03_set4_r.pdf", height = 2.5, useDingbats=FALSE)
	generatePlots("AMBE - dataset 4", AMBE, axes);
	generatePlots("MBE - dataset 4", MBE, axes);
	dev.off()
}

generatePlots <- function(title, data, axes){
	# Split into A and Abar

	# Uniform data
	A = data[which(data$trueD == unique(data$trueD)[1]),];

	# The red slab
	Abar = data[which(data$trueD == unique(data$trueD)[2]),];

	plot(A$computedD, rep(0.03, length(A$computedD)),
		xlab = "estimated density",
		ylab = " ",
		yaxt = "n",
		ylim = c(-0.05, 0.05),
		xlim = axes,
		main = title,
		col = myRed,
		pch = 3
	)
	mtext(paste("(MSE = ", signif(mean(data$MSE),3), ")"), side = 3, line = 0.3, cex = 1.5)

	points(Abar$computedD, rep(-0.03, length(Abar$computedD)),
		# xlab = "estimated density",
		# ylab = " ",
		# yaxt = "n",
		# ylim = c(-0.1, 0.1),
		# main = title,
		col = myBlue,
		pch = 3
		)

	points(A$trueD[1], 0, col = myBlue, pch = 21, bg = myBlue)
	points(Abar$trueD[1], 0, col = myRed, pch = 21, bg = myRed)
}

filename = "./03_results/03_results_N2400_set_04.mat"
par(
	mfrow = c(2,1))

handleFile(filename, 4);

