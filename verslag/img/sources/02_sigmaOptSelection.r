setwd("/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/sources/");

library(mnormt);

# Remove al existing variables
rm(list = ls())
printf <- function(...) cat(sprintf(...))

geoMean <- function(data) {
    log_data <- log(data)
    gm <- exp(mean(log_data[is.finite(log_data)]))
    return(gm)
}

sigmaFunction <- function(data){
	return(quantile(data, probs=c(0.2, 0.8), names=FALSE));
}

kernelData <- function(point, sigmaOpt){
	fac = 50;
	X = seq(point$x - fac, point$x + fac, length.out = 500);
	Y = seq(point$y - fac, point$y + fac, length.out = 500);
	kernel = function(q, t) {
		dmnorm(
			cbind(q,t), 
			mean=c(point$x[1], point$y[1]), 
			matrix(c(sigmaOpt * sigmaOpt, 0, 0, sigmaOpt * sigmaOpt), 2, 2)
		)
	}
	Z = outer(X, Y, kernel);
	return(list("X" = X, "Y" = Y, "Z" = Z));
}

addKernelToPlot <- function(point, sigmaOpt){
	# Plot the selected point...
	points(point, 
		col = myRed,
		pch = 19)
	# ...and its kernel
	kernel = kernelData(point, sigmaOpt)
	levels = pretty(range(kernel$Z, finite = TRUE), 5);
	printf("Contourlines at:")
	for (i in 1:5){
		printf(" %f", levels[i]);
	}
	printf("\n")
	contour(kernel$X, kernel$Y, kernel$Z, 
		nlevels=5, 
		col=myRed, 
		# labels='', 
		# levels = c(0.0011, 0.0022, 0.0033, 0.0044, 0.0055),
		add=TRUE);	
}

generatePlot <- function(kernelPoints, sigmaOpt, title = ''){
	# pdf(title);

	# Plot the points
	plot(points,
		pch='.', 
		col="black", 
		type='p',
		main = title,
		# ann=FALSE,
		# xaxt = 'n',
		# yaxt = 'n',
		xlim = c(-10,30),
		ylim = c(-10,30),
		xlab = 'x',
		ylab = 'y',
		mar = c(0, 0, 5, 0)
	)
	so = signif(sigmaOpt, digits = 4);
	txt <- bquote(paste(sigma[opt], " = ", .(so), sep=""));
	mtext(txt, side = 3, line = 0.3, cex = 1.5)
	for (i in 1:(dim(kernelPoints)[1])){
		addKernelToPlot(kernelPoints[i,], sigmaOpt)	
	}
	# dev.off();
}

# Define colours
myGreen <- rgb(10, 103, 58, max= 255);
myBlue <- rgb(43, 56, 143, max= 255);
myRed <- rgb(190, 30, 45, max= 255);

# Sample points from a wall and a sphere
N = 2400

wallMean = c(10,10);
wall = rmnorm(	N, 
				wallMean, 
				matrix(c(0.01, 0, 0, 5), 2, 2)
			  ) 

sphereMean = c(22,10);
sphere = rmnorm(	
			N, 
			sphereMean, 
			matrix(c(5, 0, 0, 5), 2, 2)
		  ) 
# Combine the two
points = rbind(wall, sphere)
wall.df = data.frame(x = wall[,1], y = wall[,2]);
sphere.df = data.frame(x = sphere[,1], y = sphere[,2]);


# Compute sigmaOpts
sigmas = sigmaFunction(points);
sigmaOpt.min = min(sigmas)
sigmaOpt.gmean = geoMean(sigmas)

# Select the points where the kernels are shown
wall.point = head(subset(wall.df, 
	x >= wallMean[1] - 0.1 & 
	x <= wallMean[1] + 0.1 & 
	y >= wallMean[2] - 0.2 &
	y <= wallMean[2] + 0.2) ,1)
sphere.point = head(subset(sphere.df, 
	x >= sphereMean[1] - 0.1 & 
	x <= sphereMean[1] + 0.1 & 
	y >= sphereMean[2] - 0.1 &
	y <= sphereMean[2] + 0.1) ,1)

kernelPoints = rbind(wall.point, sphere.point)

par(
	# mfrow = c(1,1)
	mfrow = c(1,2)
)
printf("--MIN--\n")
printf("Sigma opt min: %f\n", sigmaOpt.min)
generatePlot(kernelPoints, sigmaOpt.min, '../02_sigmaOpt_Min.pdf')

printf("\n--GMEAN--\n")
printf("Sigma opt gmean: %f\n", sigmaOpt.gmean)
generatePlot(kernelPoints, sigmaOpt.gmean, '../02_sigmaOpt_Gmean.pdf')

