# Remove al existing variables
rm(list = ls())

#Based on wand p 12 and http://stats.stackexchange.com/questions/70855/simulating-random-variables-from-a-mixture-of-normal-distributions

# Define colours
myGreen <- rgb(10, 103, 58, max= 255);
myBlue <- rgb(43, 56, 143, max= 255);
myRed <- rgb(190, 30, 45, max= 255);

# Load libraries
library(KernSmooth)

plotDensity <- function(width, truth, fileName, rand.samples){
	# Estimate density
	rand.density <- bkde(x=rand.samples, bandwidth = width)

	# pdf(paste('/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/01_bandwith_', fileName, '.pdf', sep=""));d
		par(
			mar = c(3.9, 3.9, 0.1, 0.1),
			lwd = 2,
			cex=1.5
		)

		# Density
		plot(rand.density, 
			type='l', 
			col=myGreen,
			ylim = c(0,0.55),
			xlim = c(-3,3),
			xlab='x',
			ylab='Density')

		x = seq(-3,3,.01)

		#True Density
		lines(x, truth)
	# dev.off();
}

# Set the directory
setwd("/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/sources/");


#The number of samples from the mixture distribution
N = 10000              

#Sample N random uniforms U
U =runif(N, -3, 3);

rand.samples = c();
for(i in 1:N){
    if(U[i]<.75){
        rand.samples[i] = rnorm(1,0,1)
    }else{
        rand.samples[i] = rnorm(1,3/2,1/3)
    }
}

# rand.samples = 3/4 * dnorm(U) + 1/4 * dnorm(U, 3/2, 1/3);

# Get true density
x = seq(-3,3,.01)
truth = .75 * dnorm(x) + .25 * dnorm(x, 3/2, 1/3)

plotDensity(0.06, truth, 'undersmoothing', rand.samples);
plotDensity(0.54, truth, 'oversmoothing', rand.samples);
plotDensity(0.18, truth, 'correct', rand.samples);