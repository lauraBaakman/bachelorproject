%!TEX root = report.tex
\chapter{Proposed Method}
\label{sec:method}

	This chapter starts by discussing some existing code bases for kernel density estimation, and explains why we have chosen to adapt the \mbe. \Cref{s:2:AMBE} introduces the AMBE algorithm and discusses each step it takes. \Cref{s:2:algorithm} presents and motivates the algorithm used to adapt the shape of the kernels.

	\section{Existing code bases}
		Before deciding on a density estimator to base our method on, we looked into some existing libraries that use \textcite{matlab}. 

		The library `Kernel Density Estimation Toolbox for MATLAB' \cite{kdeToolbox} is quite extensive but is written as a mix of MATLAB and MEX/C++ code whereas we preferred to work with only MATLAB code. Since most algorithms needed are already build into MATLAB.

		The \texttt{kernlden2d} library by \textcite{mohammadi2009KERNLDEN2D} performs 2D kernel regression well, but was not documented. Furthermore extending this method so that it could handle multiple dimensions would require a complete rewriting of the code.

		Another possibility is the \texttt{kde} library by \textcite{rasmussen2001kde}. His code is well documented and can be applied to multivariate datasets but uses a Newton scheme to determine the width of the kernels. Newton's optimization when applied to kernel density estimation iteratively tries to minimize some error function. We however preferred a plug-in estimator. 

		We also considered the code by \textcite{shi2012repo} written in support of their paper `Kernel Bandwidth Estimation in spike rate estimation'\cite{shimazaki2010kernel}. Although this code is well documented and has already proven its worth in an academic setting, it is only capable of performing kernel density estimation for univariate time series. Since we want to look at multidimensional data, this code base was not a good fit.

		Eventually we decided to adapt the Modified Breiman Estimator based on the findings of \citeauthor{ferdosi2011comparison}, who state that: ``[the] MBE is our preferred density estimator. It produces densities that are consistent with expectations from literature and provides more discriminating power than the other methods. Furthermore it is the fastest method of our tests."

	\section{AMBE}
		\label{s:2:AMBE}
		The kernel of the \ambe modifies its shape based on local data. Refer to \autoref{fig:2:kernelMorphing} for a comparison of the kernels used by the \mbe (\ref{subfig:2:MBEKernels}) and the \ambe (\ref{subfig:2:AMBEKernels}). The kernels used by the \mbe all have the same shape and are only smoothed by the parameter $\gamma_i$, as defined in \autoref{eq:1:MBEgamma}. Whereas the AMBE kernels are all shaped differently. 

		\begin{figure}[b!]
			\centering
			\begin{subfigure}[b]{0.47\textwidth}
				\centering
				\includegraphics[width=\textwidth,left]{./img/02_mathBasis_MBEKernels}
				\caption{MBE}
				\label{subfig:2:MBEKernels}
			\end{subfigure}
			\begin{subfigure}[b]{0.47\textwidth}
				\centering
				\includegraphics[width=\textwidth,right]{./img/02_mathBasis_AMBEKernels}
				\caption{AMBE}
				\label{subfig:2:AMBEKernels}
			\end{subfigure}		
			\caption{Comparison of the kernels used during the final density estimation when using (\subref{subfig:2:MBEKernels}) the \mbe and the (\subref{subfig:2:AMBEKernels}) \ambe. The MBE kernels have the same shape but a different size, whereas the AMBE kernels not only differ in size but also in shape. Data points are represented as dots. For each data point a contour line of its kernel is drawn. Note that the number of presented data points is too small to be used to accurately perform kernel density estimation. }
			\label{fig:2:kernelMorphing}
		\end{figure}

		This section presents the general AMBE algorithm. \Cref{ss:2:optimalPilotWindowWidth} through \ref{ss:2:finalEstimatedDensity} discuss the steps the algorithm takes and any changes, other than the kernels, that were made to the \mbe. 

		\Cref{ss:2:mbe} reviews how we adapted the algorithm presented in this section to use it for the \mbe.


		\begin{algorithm}
			\setstretch{1.2}
			\SetAlgoShortEnd
			\DontPrintSemicolon
			\SetKwInOut{Input}{input}\SetKwInOut{Output}{output}
			\Input{	$data$ contains $N$ $d$-dimensional observations.\\
					$\alpha$ is the sensitivity parameter} 
			\Output{$density$ is a vector of length $N$ with a density estimate of each observation in $data$.}
			\BlankLine

			\ForEach{dimension $l$ in $data$}{
				$\sigma[l]$ := \FuncSty{pilotWindowWidth}($l$) \tcc*{\autoref{eq:1:MBEsigmaL}} \label{alg:2:ambe:sigmal}
			}
			$\sigma_{opt}$ := \FuncSty{geometricMean}($\sigma$) \tcc*{\autoref{eq:1:geometricMean}} \label{alg:2:ambe:sigmaOpt}

			\BlankLine
			$\Sigma$ := \FuncSty{diag}(${\sigma_{opt}}^{2}$, $d$)\; \label{alg:2:ambe:covMatPilotD}
			\ForEach{observation $\vec{r}$ in $data$}{
				$pilotD$[$\vec{r}$] := \FuncSty{kde}($\vec{r}$, $data$, $\Sigma$) \tcc*{\autoref{eq:2:density}} \label{alg:2:ambe:pilotDensity}
			}

			\BlankLine
			\ForEach{observation $\vec{r}$ in $data$}{
				$\Gamma[\vec{r}]$ := \FuncSty{localBandwidth}($\vec{r}$, $pilotD[\vec{r}]$, $\alpha$) \tcc*{\autoref{eq:1:MBEgamma}} \label{alg:2:ambe:gamma}
			}

			\BlankLine
			$k$ := \FuncSty{ceil}(\FuncSty{sqrt}(N))\; \label{alg:2:ambe:k}
			$\Sigma$ := \FuncSty{kernelShape}($\vec{r}$, $data$, $k$, $\sigma_{opt}$) \tcc*{\Cref{alg:2:kernelshape}} \label{alg:2:ambe:kernelShape}
			\ForEach{observation $\vec{r}$ in $data$}{
				$density[\vec{r}]$ := \FuncSty{kde}($\vec{r}$, $data$, $\Gamma[r]^2 \cdot \Sigma$); \tcc*{\autoref{eq:2:density}} \label{alg:2:ambe:density}
			}
			\caption{$ambe(data, \alpha)$\label{alg:2:ambe}}
		\end{algorithm}

		\subsection{Optimal Pilot Window Width} \label{ss:2:optimalPilotWindowWidth}
			The first three lines of \Cref{alg:2:ambe} compute $\sigma_{opt}$. The pilot window width for each dimension $l$ is computed in the same way as for the MBE, see \autoref{eq:1:MBEsigmaL}. 

			We use the geometric mean to select the optimal pilot window width, whereas \citeauthor{ferdosi2011comparison} used the minimum of the different pilot window widths. Using the minimum in a dataset where a lot of points are sampled from a very small elongated structure results in a pilot window width that is dominated by the width of this strcutrure. Whereas the geometric means allows from some influence from other dimensions as well.

			Replacing the minimum with the geometric mean resulted in better performance for both the AMBE and the MBE.

		\subsection{Pilot Density} \label{ss:2:pilotDensity}
			\Cref{alg:2:ambe:covMatPilotD} through \cref{alg:2:ambe:pilotDensity} of \Cref{alg:2:ambe} compute the pilot densities. \texttt{diag(x, a)} returns an $a \times a$ matrix with $x$ on the diagonal. This method is used to create $\Sigma$, the covariance matrix of the kernel used for the pilot densities. Since we do not use shape-adapted kernels for the computation of the pilot densities to avoid overfitting this is the same covariance matrix as used by \citeauthor{ferdosi2011comparison}. 

			The method \texttt{kde} computes the pilot density for an observation $\vec{r}$:
			\begin{align}\label{eq:2:density}
				\hat{f}_{pilot} (\vec{r}) = \frac{1}{N} \sum_{i=1}^{N} K \left( \vec{r_i} \right)
				&&
				K = \norm{d}{\vec{r}}{\Sigma}.
			\end{align}
			We have chosen to use the normal kernel originally used by \citeauthor{breiman1977variable} instead of the Epanechnikov kernel used by \citeauthor{wilkinson1995dataplot}. One reason for this change was that with the normal distribution it is easier to inspect the kernels visually. Another factor was ease of implementation due to larger support of Gaussians in popular tools. Additionally, changing the shape of the normal distribution is as trivial as plugging in a different covariance matrix, whereas changing the shape of the Epanechnikov kernel is a bit harder to do.

			\citeauthor{wilkinson1995dataplot} computed the pilot densities on a grid that was placed over the data. The pilot density of each observation was obtained by multi-linear interpolation over the computed pilot densities of the grid points. We have opted to omit the grid and compute the pilot densities for each observation directly.

		\subsection{Local Bandwidth} \label{ss:2:localBandwidth}
			The loop that starts on \cref{alg:2:ambe:gamma} computes the local bandwidth for each observation $\vec{r}$ according to \autoref{eq:1:MBEgamma}. Comparing results with $\alpha = \rfrac{1}{3}$, as used by \citeauthor{ferdosi2011comparison}, and $\alpha = \rfrac{1}{5}$, suggested by \textcite{parzen1962estimation}, showed that the lower $\alpha$ yielded better results. We have thus opted to use $\alpha = \rfrac{1}{5}$.


		\subsection{Estimated Density} \label{ss:2:finalEstimatedDensity}
			On \cref{alg:2:ambe:k} the parameter $k$ is computed, this parameter is discussed in detail in \cref{ss:2:kernelShape}. For each observation $\vec{r}$ a \texttt{kernelShape} is computed, the algorithm used here is the subject of \cref{s:2:algorithm}.

			The density estimation for each data point is computed with the method $\texttt{kde}$ according to \Cref{eq:2:density}.

		\subsection{MBE} \label{ss:2:mbe}
			As stated earlier we have also estimated the densities with our version of the \mbe to be able to compare performance. We have computed the results of the MBE by excuting \Cref{alg:2:ambe} by replacing \cref{alg:2:ambe:k} through \cref{alg:2:ambe:density} with the code presented in \Cref{alg:2:mbeAdaption}.

		\begin{algorithm}[H]
			\setstretch{1.2}
			\DontPrintSemicolon
			\ForEach{observation $\vec{r}$ in $data$}{
				$\Sigma$ := \FuncSty{diag}(${\left( \sigma_{opt} \cdot \Gamma[\vec{r}]\right)}^{2}$, $d$)\; \label{alg:2:mbe:covMatDensity}
				$density[\vec{r}]$ := \FuncSty{kde}($\vec{r}$, $data$, $\Sigma$); \tcc*{\autoref{eq:2:density}} \label{alg:2:mbe:density}
			}
			\caption{The code that replaced \cref{alg:2:ambe:k} through \cref{alg:2:ambe:density} in \Cref{alg:2:ambe} so that it could be used for the \mbe.\label{alg:2:mbeAdaption}}
		\end{algorithm}		

	\section{Kernel Shape}
		\label{s:2:algorithm}
		\Cref{alg:2:kernelshape} shows the algorithm used for the modification of the kernels. \Cref{sss:2:neighbourhood} through \ref{sss:2:scalingFactor} discusses the steps taken in detail.

		\begin{algorithm}[H]
			\setstretch{1.2}
			\DontPrintSemicolon
			\SetKwInOut{Input}{input}\SetKwInOut{Output}{output}
			\Input{	$\vec{r}$ is the observation for which the kernel shape is computed.\\
					$data$ contains the observations.\\ 
					$k$ denotes the number of neighbours to take into account.\\
					$\sigma_{opt}$ is the optimal smoothing parameter.}
			\Output{$\Sigma$ is the matrix that shapes the kernel.}
			\BlankLine
			$C_k$ := (\FuncSty{kNearestNeighbours}($\vec{r}$, $data$, $k - 1$)) $\bigcup$ $\vec{r}$; \label{alg:2:kernelshape:knn}

			$covMat$ := \FuncSty{cov}($C_k$); \label{alg:2:kernelshape:cov}

			$\{\lambda_1, \ldots, \lambda_d\}$ :=  \FuncSty{eigenValues}(covMat); \label{alg:2:kernelshape:eigenvalues}

			$S$ := \FuncSty{scalingFactor}($\{\lambda_1, \ldots, \lambda_d\}$, $\sigma_{opt}$); \tcc*{\autoref{eq:2:scalingFactorMultiple}} \label{alg:2:kernelshape:scaling}

			$\Sigma$ := $S \cdot$ covMat; \label{alg:2:kernelshape:kernelshape}

			\caption{$kernelShape(\vec{r}, data, k, \sigma_{opt})$\label{alg:2:kernelshape}}
		\end{algorithm}

		\subsection{Neighbourhood}\label{sss:2:neighbourhood}
			%Intro
			The first step of \Cref{alg:2:kernelshape} (\autoref{alg:2:kernelshape:knn}) determines the neighbourhood of the current data point $\vec{r}$. These neighbouring data points are used to determine the shape of the kernel of $\vec{r}$.

			% Algoritme selectie
			We have opted to use the $k$-nearest neighbours ($k$-NN) algorithm instead of fixed-radius near neighbours (FNN). Using $k$-NN rather than FNN ensures that no matter how sparse the dataset is, the shape of the kernel is always based on a reasonable number of points, assuming a reasonable $k$ has been selected. The fixed-radius near neighbours algorithm however does not guarantee that it returns a reasonable number of neighbours. In a sparse dataset it is conceivable that the kernel shape of a data point would be based on just the point itself. \autoref{fig:2:neighbourhood} provides a visual comparison of the two algorithms.

			Another advantage of using $k$-NN over FNN is that the $k$ can be selected to be higher than $d + 1$. This makes it extremely improbable that the covariance matrix of this neighbourhood, used in the next step of the algorithm, is singular. A singular covariance matrix would be a problem since they do not have eigenvalues, which are necessary for the computation of the shape-adapted kernel. 

			\begin{figure}
				\centering
				\begin{subfigure}[b]{0.45\textwidth}
					\centering
					\includegraphics[width=0.9\textwidth]{./img/02_mathBasis_FNN}
					\caption{FNN}
					\label{subfig:2:FNN}
				\end{subfigure}
				\begin{subfigure}[b]{0.45\textwidth}
					\centering
					\includegraphics[width=0.9\textwidth]{./img/02_mathBasis_KNN}
					\caption{$k$-NN}
					\label{subfig:2:KNN}
				\end{subfigure}	
				\caption{Difference between determining neighbourhoods between fixed-radius near neighbours ($\delta = 150$) and K-nearest neighbours ($k = 4$) for data point $\vec{r}$.}
				\label{fig:2:neighbourhood}
			\end{figure}
			% K
			The number of data points selected by $k$-nearest neighbours depends on the value selected for $k$. If the local data are sparse, $k$-NN selects a large neighbourhood. In a dense region of the data the data points returned by $k$-NN come from a small area. 

			The data point $\vec{r}$ is included in $C_k$ so that it is used as well in the computation of its kernel shape.

			\textcite{Russell1995ArtificalCH20} recommend a $k$ between five and ten for most low-dimensional datasets. \citeauthor{silverman1986density} suggests that $k = \sqrt{N}$ is reasonable when using the $k$-nearest neighbour algorithm. Since we require the neighbours to reflect the shape of the local data, and this cannot be done with just five to ten oberservations, a large $k$ works better in our case. Therefore we have followed \citeauthor{silverman1986density}'s recommendation.

			% Distance metric
			To determine the nearest neighbours of any observation $\vec{r}$, a distance metric $D(\vec{r}, \vec{r_i})$ is needed to compute the distance between observation $\vec{r}$ and $\vec{r_i}$. We have opted to use euclidean distance.

		\subsection{Covariance} \label{sss:2:covariance}
			We describe the variance of the subset $C_k$ with the dispersion matrix \texttt{covMat}, which is computed in \autoref{alg:2:kernelshape:cov} of \Cref{alg:2:kernelshape}.

			A multivariate Gaussian, our chosen kernel, is defined by its covariance matrix $\left(\Sigma \in R^{d \cdot d}\right)$ and its mean $(\mu \in R^d)$. Consequently the kernel \norm{d}{\vec{r}}{\Sigma} should fit the shape of the neighbourhood $C_k$. \autoref{fig:2:EigenValues} shows the contour lines of a kernel that was defined like this. This image clearly illustrates that the shape of the kernel conforms to the shape of the data.

			\autoref{subfig:2:AMBEKernels} shows one contour line for each kernel applied to the observations. When comparing the areas of these kernels it is clear that they differ based on their shapes. To allow the density estimation of each point to be influenced by a equal area of points, the areas of the kernels need to be identical. This is achieved by scaling each kernel. The computation of the necessary scaling factor is discussed in \cref{sss:2:scalingFactor}.

			\begin{figure}[ht]
			\centering
				\centering
				\fbox{\includegraphics[width=0.95\textwidth]{./img/02_mathBasis_EigenValues}}
				\caption{This figure shows 20 000 points sampled from a bivariate Gaussian (\norm{2}{[2,3]}{[1, 1.5; 1.5, 3]}). The eigenvectors of the covariance matrix $\Sigma_{20000}$ of these points, shown in blue, are scaled by the square roots of the eigenvalues so that they form the axes of the eigen ellipse, which is shown in blue. The vectors are then shifted so that their tails originate from the mean of the data. The isolines of the kernel centred on the mean of the data with the dispersion matrix $\Sigma_{20000}$ are shown in red.}
				\label{fig:2:EigenValues}
			\end{figure}

		\subsection{Eigenvalues} \label{sss:2:eigenValues}
			The principal components of $C_k$ encode the variability of the data in an orthogonal basis. The principal components can be computed by calculating the eigenvectors of the dispersion matrix \texttt{covMat}. The eigenvector with the highest eigenvalue explains most of the variance of the data. \cite{jolliffe2005principal}. Since the eigenvectors are always perpendicular, they can be interpreted as the major and minor axes of an ellipse, in the two-dimensional case. The blue oval shape in \autoref{fig:2:EigenValues} is an example of such an ellipse. We call such an ellipse an eigen ellipse.

			The contour lines of the kernels used by the \mbe for both the pilot density and the final density are circles, e.g. \autoref{subfig:2:MBEKernels}. The \ambe uses the same procedure as the \mbe for the pilot density and thus the contour lines of these kernels are also shaped like circles. AMBE's final density estimation however is computed using shape-adapted kernels, their contour lines are ellipses, as shown in \autoref{subfig:2:AMBEKernels}.

			Comparing the shape of the eigen ellipse (shown in blue in \autoref{fig:2:EigenValues}) with the isolines of the kernel defined by the covariance matrix of the data $\Sigma_{20000}$ (shown in red) we see that they are similar in shape.

			We use this equality in \cref{sss:2:scalingFactor} to compute the scaling factor we have mentioned in \cref{sss:2:covariance}.

		\subsection{Scaling Factor} \label{sss:2:scalingFactor}
			% The eigenvalues of the covariance matrix encode the variability of the data in an orthogonal basis that captures as much of the data's variability as possible in the first few basis functions (aka the principle component basis).

			%Waarom S
			\autoref{subfig:2:AMBEKernels} illustrated that the areas of the shape-adapted kernels at a certain level differed from each other, even before the application of the smoothing parameter $\gamma$. We wish to derive a scaling factor $S$ that scales the kernels so that their areas at this level are equal, before the application of the smoothing parameter. This ensures that the kernels used by AMBE and MBE are smoothed equally and that the only difference between the two estimators are the kernel shape. 

			%Introduceer basis area en oppervlakte
			We take the area of the contour line with radius $\sigma_{opt}$ of the non-smoothed MBE kernel as the guiding area. This area, $A_{MBE}$, has size: ${\sigma_{opt}}^2 \cdot \pi$. Since the contour lines of the shape-adapted kernel are equal in shape to the eigen ellipse, we also know the size of the area defined by an isoline of the kernel: $A_{AMBE} = \sqrt{\lambda_1} \cdot \sqrt{\lambda_2} \cdot \pi$. We can find a scaling factor that ensures that the areas are equal by solving this equation for $S$:
				\begin{equation}\label{eq:2:scalingFactor}
					A_{MBE} = S \cdot A_{AMBE}
				\end{equation}
			Generalizing to $d$-dimensions the scaling factor $S$ is defined as:
			\begin{equation}\label{eq:2:scalingFactorMultiple}
				S = \frac{{\sigma_{opt}}^2}{\gm(\{\sqrt{\lambda_1}, \ldots, \sqrt{\lambda_d}\})}.
			\end{equation}	

			Multiplying the computed scaling factor with the covariance matrix \texttt{covMat} (\autoref{alg:2:kernelshape:kernelshape}) gives the scaled matrix. The resulting kernel ($\norm{d}{\vec{r}}{\Sigma}$) is then smoothed by multiplying with the smoothing parameter, $\gamma$, squared, see \autoref{alg:2:ambe:density} in \Cref{alg:2:ambe}.