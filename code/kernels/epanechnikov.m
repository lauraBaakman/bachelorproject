function res = epanechnikov(x)
    %EPANECHNIKOV The epanechnikov kernel 
    % INPUT
    %   - x The vector inputted in the kernel
    % OUPUT
    %  - Height of the vector x in the kernel
    
    dotprd = dot(x,x);
    
    res = 0;
    
    if (dotprd < 1) 
        dim = length(x);
        Vd = unitSphereVolume(dim);
        res = ((dim + 2) / (2 * Vd)) * (1 - dotprd);
    end 
end

