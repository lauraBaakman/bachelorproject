function sigma = percentile(data, lowerPercentile, upperPercentile)
    %PERCENTILE This function computes a sigma for one variable using
    %percentiles.
    % INPUT
    %   - data:         a vector of data
    %   - lowerPercentile  The lower percentile
    %   - upperPercentile  The upper percentile
    % OUTPUT
    %   - sigma         The computed value for sigma
   
    
    sigma = (prctile(data, upperPercentile) - prctile(data, lowerPercentile)) / log (length(data));
end

