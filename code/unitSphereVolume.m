function volume = unitSphereVolume(dim)
    %UNITSPHEREVOLUME Computes the volume of the unitsphere
    %   INPUT
    %   - dim: The dimension of the unit sphere
    %   OUTPUT
    %   - volume:   The volume of the unitsphere in dimension dim

    % source: http://www.math.hmc.edu/funfacts/ffiles/30001.2-3.shtml
    
    teller = (pi^(dim / 2));
    
    noemer = (gamma(dim/2 + 1));

    volume = teller / noemer;
 
end

