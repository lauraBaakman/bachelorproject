function plotResults(trueValues, density)
    %PLOTRESULTS Plots the results of the kernel density estimation
    %   Detailed explanation goes here
    
%     figure('Name','Overview','NumberTitle','off')
%     if data.dim == 1
%         hold on
%         % The data
%         scatter(data.getVariable(1), zeros(data.N, 1), 'x');
%         % The density
%         scatter(data.getVariable(1), density, 7, 'MarkerEdgeColor', 'r');
%         % The pilot density
%         scatter(data.getVariable(1), pilotDensity, 2, 'MarkerEdgeColor', 'green');
%         
%         % The true values
%         scatter(data.getVariable(1), trueValues, 1);
%         
%         % Legend
%         legend('data', 'density estimate', 'pilot density', 'true values');
%         hold off
%     else if data.dim == 2
%             hold on
%             %The data
%             x = data.getVariable(1);
%             y = data.getVariable(2);
%             z = zeros(data.N, 1);
%             p_data = scatter3(x, y, z, 'x');
%             
%             % The density
%             p_density = scatter3(data.getVariable(1), data.getVariable(2), density, 'MarkerEdgeColor', 'r');
%             
%             % The pilot density
%             p_pilotD = scatter3(data.getVariable(1), data.getVariable(2), pilotDensity, 'MarkerEdgeColor', 'green');
%             
%             % The true values
%             p_trueValues = scatter3(data.getVariable(1), data.getVariable(2), trueValues);
%             
%             % The true density
%             colormap gray
%             x1 = -3:.2:3;
%             [X1,X2] = meshgrid(x1,x1);
%             F = mvnpdf([X1(:) X2(:)],mu,sd);
%             F = reshape(F,length(x1),length(x1));
%             surf(x1,x1,F);
%             caxis([min(F(:))-.5*range(F(:)),max(F(:))]);
%             axis([-3 3 -3 3 0 .4])
%             xlabel('x1'); ylabel('x2'); zlabel('Probability Density');
%             
%             legend('data', 'density estimate', 'pilot density', 'true values');
%             
%             grid on; view(3);
%             hold off
%             
%             set(p_pilotD,'Visible','off');
%         else
%             disp('Geen plots gedefinieerd voor deze dimensie.');
%         end
        
        figure('Name','True versus estimated density','NumberTitle','off')
        p_trueVsEstimated = scatter(trueValues, density, 'MarkerEdgeColor','b', 'MarkerFaceColor','b');
        xlabel('true density');
        ylabel('estimated density');
        set(gca, 'XTickLabelMode', 'manual', 'XTickLabel', []);
        set(gca, 'YTickLabelMode', 'manual', 'YTickLabel', []);
        
    end
    
