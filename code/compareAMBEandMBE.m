function [AMBE, MBE, data, trueDensities] = compareAMBEandMBE(dataGenerator, N)
    %COMPAREAMBEANDMBE Compare the results of AMBE and MBE
    % INPUT
    %   - dataGenerator: The dataset to compare AMBE and MBE on
    %   -N: The number of datapoints to consider during the KDE.
    % OUTPUT
    %   - AMBE: Structure with the avarage MSE, the mean MSE, the estimated
    %   densities and the true densities when using AMBE.
    %   - MBE: Structure with the avarage MSE, the mean MSE, the estimated
    %   densities and the true densities when using MBE.
    
    %% Generate Data
    fprintf('\t\nGenerating data..\n');
    [data, trueDensities] = dataGenerator(N);
    
    %% AMBE
    fprintf('\tAMBE..\n');
    [computedDensities] = kde(data, N, false);
    [MSE, MSES] = mse(computedDensities, trueDensities);
    AMBE = struct('MSE', MSE, 'MSES', MSES, 'computedDensities', computedDensities, 'trueDensities', trueDensities);
    
    %% MBE
    fprintf('\tMBE..\n');
    [computedDensities] = kde(data, N, true);
    [MSE, MSES] = mse(computedDensities, trueDensities);
    MBE = struct('MSE', MSE, 'MSES', MSES, 'computedDensities', computedDensities, 'trueDensities', trueDensities);
end

