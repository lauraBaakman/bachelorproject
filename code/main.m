%% Start with a blank slate
clear all; close all force; clc;

%% Add paths
addpath('datagenerators');

%% Parameters
N = 2400; 
curDate = datestr(now, 'mmddHHMM');
basename = strcat('./results/', curDate, '_results_N', int2str(N), '_set_0');

%% datasets
gen = {@set1, @set2, @set3, @set4, @set5, @set6, @set7, @set8};

tic
for i = 1:length(gen)
    fprintf( 'Dataset %d', i);
    [AMBES, MBES] = compareAMBEandMBE(gen{i}, N);    
    save(strcat(basename, int2str(i), '.mat'), 'AMBES', 'MBES', '-v6');
end
toc

beep; 