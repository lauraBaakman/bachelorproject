function [ lambdas ] = localBandwidthParameters( pilotDensities, alpha )
    %LOCALBANDWIDTHPARAMETERS Computes the local bandwidth parameters
    % INPUT
    % - pilotDensities: A (N x 1) matrix of pilotdensities
    % - sensitivity parameter
    % OUTPUT
    % - lambdas: A (N x 1) matrix of local bandwidth parameters
    
    % Compute the geometric mean
    g = geomean(pilotDensities);
    
    % Compute the bandwidth parameter for each point
    lambdas = (pilotDensities ./g ) .^ alpha;    
    
end

