%% Init
% Add other folders to path
addpath('datagenerators');
addpath('errorMeasures');

% Number of points
N = 4800;


% Basename of each file
path = '/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/sources/02_datasets/';
basename = strcat(path, 'N', int2str(N), '_set');

% Files generated with a loop can't be read by R for some particual reason
[data, tv] = set1(N);
data = data.observations;
fa = fractionalAnisotropyOfSet(data);
name = strcat(basename, '1.mat');
% save(name, '-v6', 'data', 'N', 'fa');
save(name, '-v6', 'data', 'N', 'tv', 'fa');

[data, tv] = set2(N);
data = data.observations;
fa = fractionalAnisotropyOfSet(data);
name = strcat(basename, '2.mat');
% save(name, '-v6', 'data', 'N', 'fa');
save(name, '-v6', 'data', 'N', 'tv', 'fa');

[data, tv] = set3(N);
data = data.observations;
fa = fractionalAnisotropyOfSet(data);
name = strcat(basename, '3.mat');
% save(name, '-v6', 'data', 'N', 'fa');
save(name, '-v6', 'data', 'N', 'tv', 'fa');

[data, tv] = set4(N);
data = data.observations;
fa = fractionalAnisotropyOfSet(data);
name = strcat(basename, '4.mat');
% save(name, '-v6', 'data', 'N', 'fa');
save(name, '-v6', 'data', 'N', 'tv', 'fa');

[data, tv] = set5(N);
data = data.observations;
fa = fractionalAnisotropyOfSet(data);
name = strcat(basename, '5.mat');
% save(name, '-v6', 'data', 'N', 'fa');
save(name, '-v6', 'data', 'N', 'tv', 'fa');