path = '/Users/laura/Dropbox/Studie/Bachelorproject/verslag/img/sources/03_results/';
baseFileName = '03_results_N2400_set_0';

for i = 2:5
   filename = strcat(baseFileName, int2str(i), '.mat');
   filepath = strcat(path, filename);
  
   load(filepath);
   
   fa = fractionalAnisotropyOfSet(data);
   save(filepath, '-v6', 'AMBE', 'MBE', 'data', 'fa');
end
