%% Start with a blank slate
clear all; close all force; clc;

%% Add paths
addpath('datagenerators');

%% Parameters
N = 1800; 
%% datasets
gen = {@set1, @set2, @set3, @set4, @set5};

matlabpool('open', 4)

numSets = length(gen);

parfor i = 1:numSets
    fprintf( 'Dataset %d', i);
    [AMBES, MBES, data, trueDensities] = compareAMBEandMBE(gen{i}, N);    
    saveFunction(AMBES, MBES, N, i, data.observations, trueDensities);
end

matlabpool('close')

beep; 