classdef dataObject
    %DATAOBJECT Class to easlily handle the data. A set of observation is represented by a matrix where each row is an observation and each column represents a variable
    
    properties
        % The matrix with obeservations, each row is an observation, each
        % column a variable.
        observations;
        % The number of variables per observation
        dim;
        % The number of datapoints
        N;
    end
    
    methods
        
        function obj = dataObject(value)
            %OBSERVATION Constructor to create an observation
            % INPUT
            %   - input1: A matrix where each row represents an observation, each column a
            %   variable. If the function has two inputs, input1 is the
            %   number of rows in the matrix to be generated.
            % OUTPUT
            % - obj: An observation object without optimalSigma computed
            obj.observations = value';
            [obj.N, obj.dim] = size(obj.observations);
        end %constructor
        
        function element = getElement(obj, row, column)
            %ELEMENT: Returns the element in the observation matrix in row
            %row and column column
            % INPUT
            %   - obj: The object
            %   - row: The row from the element, this may be a vector or
            %   a scalar.
            %   - column: The column from the element, this may be a vector
            %   or a scalar
            % OUTPUT
            %   - element: The element found at the inputted index/indices
            element = obj.observations(row, column);
        end %element
        
        function col = getVariable(obj, index)
            % GETVARIABLE Returns the variable from column index
            % INPUT
            %    - obj: The object
            %    - index: The index of the requested variable, or a list of
            %    indices.
            % OUTPUT
            %    - col: The column with the values of the requested variable
            col = obj.observations(:, index);
        end % getVariable
        
        function row = getObservation(obj, index)
            % GETOBSERVATION Returns the observation from row index
            % INPUT
            %    - obj: The object
            %    - index: The index of the requested observation.
            % OUTPUT
            %    - row: The row with the values of the requested observation
            row = obj.observations(index, :);
        end % getObservations
        
    end
    
end

