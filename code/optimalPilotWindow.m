function optimalSigma = optimalPilotWindow(data, sigmaFunction )
    %OPTIMALPILOTWINDOW computes the optimal pilot window
    % INPUT
    %   - sigmaFunctionHandle: A handle to a function in the folder
    %   sigmaFunctions. This function should have the signature = x
    % y = sigmaFunctionHandle(x) where x is a vector and y a
    % scalar.
    %   - data is a dataObject.
    % OUTPUT
    %   - sigmaOpt: the optimal sigma
    
    sigmas = zeros(data.dim, 1);
    
    % Compute the sigma for each dimension
    for l = 1:data.dim
        sigmas(l) = sigmaFunction(data.getVariable(l));
    end
    
    optimalSigma = geomean(sigmas);
end

