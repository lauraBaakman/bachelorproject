%% Start with a blank slate
clear all; close all force; clc;

%% Add paths
addpath('datagenerators');

%% Init
gen = {@set1, @set2, @set7, @set13, @set14};
N = 30000;

%% Plot
for i = 1:length(gen)
    figure();
    data = gen{i}(N); 
    scatter3(data.getVariable(1), data.getVariable(2), data.getVariable(3), 1.5);
    title(strcat('Dataset', int2str(i)));
    axis([0 50 0 50 0 50])
    xlabel('x');
    ylabel('y');
    zlabel('z');
    min(data.observations)
    max(data.observations)
end