%% Start with a blank slate
clear all; close all force; clc;

%% Add paths
addpath('datagenerators');
addpath('sigmaFunctions');
addpath('kernels');
addpath('errorMeasures');

%% Parameters
N = 600;
basename = '~/Desktop/result_set_0';

%% datasets
gen = {@set1, @set2, @set3, @set4, @set5, @set6};

for mbe= 0:1
    for i = 1:length(gen)
        fprintf('\t\tSet %d..\n', i)
        
        %% Generate data
        [data, trueDensities] = gen{i}(N);
        
        %% Parameters
        lowerPercentile = 20;
        upperPercentile = 80;
        k = ceil(sqrt(N)); % Silverman: k \approx sqrt(n)
        
        %% Compute optimal sigma
        sigmaFunc = @(x) percentile(x, lowerPercentile, upperPercentile);
        sigmaOpt = optimalPilotWindow(data, sigmaFunc);
        
        %% Compute the kernels
        if(~mbe)
            ambeKernels = computeAMBEKernels(data, k, sigmaOpt);
        end
        
        %% Compute pilot densities
        lambdas = ones(data.N, 1);
        if (mbe)
            pilotDensities = estimateDensities(data, lambdas, sigmaOpt);
        else
            pilotDensities = estimateDensities(data, lambdas, sigmaOpt, ambeKernels);
        end
        [MSE, MSES] = mse(pilotDensities, trueDensities);
        
        loc = data.observations;
        save(strcat(basename, int2str(i), 'MBE=', int2str(mbe), '.mat'),'loc', 'mbe', 'MSE', 'MSES', 'N', 'pilotDensities', 'trueDensities', '-v6');
    end
end

beep;