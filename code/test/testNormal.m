x = -3:0.1:3;
[X1,X2, X3] = meshgrid(x, x, x);
x = [X1(:) X2(:) X3(:)];

mu = [1.2, 2 0.8];
sigma = 0.5;
SIGMA = [sigma^2, 0, 0; 
         0, sigma^2, 0;
         0, 0, sigma^2];

% With the standard normal distribution
convert = @(x, mu, sigma) (repmat(mu, length(x), 1) - x) ./ sigma; 

z1 = sigma^-3 * mvnpdf(convert(x, mu, sigma));

z2 = mvnpdf(x, mu, SIGMA);

hold on
plot(z1, 'LineWidth', 2)
plot(z2, 'g')
hold off