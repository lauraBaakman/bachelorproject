%% Start with a blank slate
clear all; 

%% Close all
close all force; clc;

%% Add paths
addpath('datagenerators');

%% Parameters
% Number of points
N = 500;

% Dimension
dim = 2;

k = ceil(sqrt(N));

%% Generate data
data = dataObject(mvnrnd(zeros(2,1), diag([1, 1]), N)');

%% Determine kernel
    % Select a point
    rIndex = ceil(random('Uniform', 0, N));
    r = data.getObservation(rIndex);
    
    % Compute sigma_opt
    sigmaFunc = @(x) percentile(x, 20, 80);
    sigmaOpt = optimalPilotWindow(data, sigmaFunc);
    
    % Compute kernel shape
    selectFunc = @(x) knnsearch(data.observations, x, 'k', k, 'distance', 'seuclidean');
    kernelShape = computeKernelShape(r, data, selectFunc, sigmaOpt);
    
%% Vergelijkingsmateriaal
    % D_k
    DkIndex = knnsearch(data.observations, r, 'k', k)';
    Dk = data.getObservation(DkIndex);
    
    % Cov mat en eigenValues
    covDk = cov(Dk);
    [eigenVectors, eigenValues] = eig(covDk);
    eigenValues = (diag(eigenValues));
    
    ev1x = r(1) + eigenValues(1) * eigenVectors(1,1);
    ev1y = r(2) + eigenValues(1) + eigenVectors(2,1);
    
    ev2x = r(1) + eigenValues(2) * eigenVectors(1,2);
    ev2y = r(2) + eigenValues(2) + eigenVectors(2,2);
    
%% Create the contour plot of the kernel
    % X and Y axis
    X = r(1) - 3:0.01:r(1) + 3;
    Y = r(2) - 3:0.01:r(2) + 3;
    
    % Z
    [X, Y] = meshgrid(X,Y);
    Z = mvnpdf([X(:) Y(:)],r,kernelShape);
    Z = reshape(Z,size(X));
    
    % Set colours for the scatter plot
    col = [ones(N,1), zeros(N,2)];
    col(DkIndex, :) = [zeros(k,2), ones(k,1)];
    col(rIndex, :) = [0, 1, 0];
    
    % Plot
    hold on
    contour(X, Y, Z);
    scatter(data.getVariable(1), data.getVariable(2), 50, col);
    line([r(1) ev1x], [r(2), ev1y]);
    line([r(1) ev2x], [r(2), ev2y]);
    hold off
