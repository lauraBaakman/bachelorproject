%% Start with a blank slate
clear all; close all force; clc;

%% Add paths
addpath('datagenerators');
addpath('datagenerators/transformations');

%% Parameters
% Number of points
N = 9000;

% Dimension
dim = 3;
%% Set1
    % Generate data   
    [data, dens] = set5(N);
    
    colormap('cool')
    cmap = colormap;
    colInd = ceil((dens - min(dens))/(max(dens) - min(dens)) * 63) + 1;
    col = cmap(colInd, :);
    
    figure(1)
    scatter3(data.getVariable(1), data.getVariable(2), data.getVariable(3), 5, col);
%     axis([0 100 0 100 0 100])
    xlabel('x');
    ylabel('y');
    zlabel('z');
    
%     figure(2)
%     scatter(1:length(dens), dens);
