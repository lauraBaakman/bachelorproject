%% Start with a blank slate
clear all; close all force; clc;

%% Init
% Add other folders to path
addpath('datagenerators');
addpath('errorMeasures');

% Parameters
N = 30000;

%% Generate data
[data, ~] = set1(N);
fa = fractionalAnisotropyOfSet(data.observations);

%% Plot
    colormap('cool')
    cmap = colormap;
    colInd = ceil((fa- min(fa))/(max(fa) - min(fa)) * 63) + 1;
    col = cmap(colInd, :);
    
    figure(1)
    scatter3(data.getVariable(1), data.getVariable(2), data.getVariable(3), 5, col);
%     axis([0 100 0 100 0 100])
    xlabel('x');
    ylabel('y');
    zlabel('z');