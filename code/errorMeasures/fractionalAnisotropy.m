function fracAnis = fa(eigenValues)    
    denominator = sum(eigenValues);
    mu = 1/3 * denominator;
    numerator = sqrt(sum((eigenValues - mu).^2));
    fracAnis = sqrt(3/2) * numerator/denominator;
end