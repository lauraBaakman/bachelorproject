function [fas] = fractionalAnisotropyOfSet(data)
    %FRACTIONALANISOTROPYOFSET computese the fractional anisotropy of each
    %datapoint in data with their k-nearest neighbours
    %   INPUT
    %   data: A matrix where each row reprsents an observation and each
    %   column a variable
    %   OUTPUT
    %   fa: A vector with the fractional anistropy for each datapoint
    
    %% INIT
    distanceMethod = 'seuclidean';
    N = length(data);
    k = ceil(sqrt(N));
    knn = @(x) knnsearch(data, x, 'k', k, 'distance', distanceMethod);
    fas = zeros(N, 1);
    
    h = waitbar(0,'Initializing waitbar...');
    for i = 1:N
        %Compute neighbours
        indices = knn(data(i,:));
        neighbours = data(indices,:);
        
        %Compute eigenvalues of the covariance matrix
        Sigma = cov(neighbours);
        
        eigenValues = sort(eig(Sigma));
        
        %Compute fa
        fas(i) = fractionalAnisotropy(eigenValues);
        waitbar(i/N,h, sprintf('%d%% along...', i/N));
    end 
    close(h);

