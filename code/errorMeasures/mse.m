function [ error, errors ] = mse( predicted, actual)
    %MSE Computes the mean squared error of the computedValues and
    %trueValues
    errors = (predicted - actual).^2;
    error = mean(errors); 
end

