function [ data, trueValues ] = set5( N )
    %SET7 Generates N datapoints drawn from two intersecting walls
    %   INPUT
    %   - N       The number of points to generate
    %   OUTPUT
    %   - data       A data object
    %   - trueValues The true densities of these points
    
    %% Init
    wallP = 2/5;
    uniP = 1/5;
    
    wallN = wallP * N;
    uniformN = uniP * N;    
    dim = 3;
    
    %% Wall 1
    wall1Xlower = 15;
    wall1Xupper = 35;
    
    wall1Ylower = 15;
    wall1Yupper = 35;
    
    wall1Zmean = 25;
    wall1Zvar = 1;
    
    x = uniformDist(wall1Xlower, wall1Xupper, wallN, 1);
    y = uniformDist(wall1Ylower, wall1Yupper, wallN, 1);
    z = uniformDist(wall1Zmean - sqrt(wall1Zvar), wall1Zmean + sqrt(wall1Zvar), wallN, 1);
    
    wall1D = [x, y, z];
    %% Wall 2
    wall2Xmean = 25;
    wall2Xvar = 4;
    
    wall2Ylower = 5;
    wall2Yupper = 25;
    
    wall2Zlower = 20;
    wall2Zupper = 30;
    
    x = uniformDist(wall2Xmean-sqrt(wall2Xvar), wall2Xmean+sqrt(wall2Xvar), wallN, 1);
    y = uniformDist(wall2Ylower, wall2Yupper, wallN, 1);
    z = uniformDist(wall2Zlower,wall2Zupper, wallN, 1);
    
    wall2D = [x, y, z];
    %% The uniform data
    % parameters
    lower = 0;
    upper = 50;
    
    % values
    [uniformD] = uniformDist(lower, upper, uniformN, dim);

    uniDensity = 1 / ((upper - lower) ^ dim);
    
    %% Combine uniform and normal data
    data = dataObject([wall1D; wall2D; uniformD]');
    tempdata = data.observations;
    
    % Prepare density wall1
    wall1Density = 1;
    upperWall1 = [35, 35, 26];
    lowerWall1 = [15, 15, 24];
    for i=1 : dim
       wall1Density = wall1Density/(upperWall1(i)-lowerWall1(i)); 
    end 
    wall1Space = (tempdata(:,3) < upperWall1(3)) & (tempdata(:,3) > lowerWall1(3)) &...
                 (tempdata(:,2) < upperWall1(2)) & (tempdata(:,2) > lowerWall1(2)) &...
                 (tempdata(:,1) < upperWall1(1)) & (tempdata(:,1) > lowerWall1(1));
             
    % Prepare density wall2
    wall2Density = 1;
    upperWall2 = [27, 25, 30];
    lowerWall2 = [23, 5, 20];
    for i=1 : dim
       wall2Density = wall2Density/(upperWall2(i)-lowerWall2(i)); 
    end     
    wall2Space = (tempdata(:,3) < upperWall2(3)) & (tempdata(:,3) > lowerWall2(3)) &...
                 (tempdata(:,2) < upperWall2(2)) & (tempdata(:,2) > lowerWall2(2)) &...
                 (tempdata(:,1) < upperWall2(1)) & (tempdata(:,1) > lowerWall2(1));    
    
    %Compute final density
    trueValues = wallP * wall1Density .* wall1Space + ...
        wallP * wall2Density .* wall2Space + ...
        uniP * uniDensity .* (ones(N,1));
end

