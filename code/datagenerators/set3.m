function [ data, trueValues ] = set3( N )
    %DATASET8 Generates N datapoints from  a dataset wiht three intersecting ellipsoids
    %   INPUT
    %   - N       The number of points to generate
    %   OUTPUT
    %   - data       A data object
    
    %% init
    p = 1/3;
    divN = ceil(N * p);
    
    mu = [15,15,15];
    
    %% Ellipissoid 1
    varianceEll1 = diag([1, 10, 1]);
    
    % values
    ell1D = normalDist(mu, varianceEll1, divN);
    
    %% Ellipissoid 2
    varianceEll2 = diag([10, 1, 1]);
    
    % values
    ell2D = normalDist(mu, varianceEll2, divN);
    
    %% Ellipissoid 3
    varianceEll3 = diag([1, 1, 10]);
    
    % values
    ell3D = normalDist(mu, varianceEll3, divN);
    
    %% Combine
    data = dataObject([ell1D; ell2D; ell3D]');
    trueValues = p * mvnpdf(data.observations, mu, varianceEll1) + ...
        p * mvnpdf(data.observations, mu, varianceEll2) + ...
        p * mvnpdf(data.observations, mu, varianceEll3);
    
end

