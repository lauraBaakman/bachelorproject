function [ data, trueValues ] = set1( N )
    %SET1 scaled version of dataset 1 by Ferdosi: multivariate gaussian + noise
    %   INPUT
    %   - N       The number of points to generate
    %   OUTPUT
    %   - data       A data object
    %   - trueValues The true densities of these points
    
    
    %% Init
    dim = 3;
    
    uniP = 1/3;
    normP = 2/3;
    
    uniformN = ceil(N * uniP);
    normalN  = ceil(N * normP);
    
    
    %% The normal data
    % parameters
    mu = 25 * ones(1, dim);
    variance = diag(15 * ones(dim,1));
    
    % values
    [normalD] = normalDist(mu, variance, normalN);
    
    %% The uniform data
    % parameters
    lower = 0;
    upper = 50;
    
    % values
    [uniformD] = uniformDist(lower, upper, uniformN, dim);
    
    %% Combine uniform and normal data
    data = dataObject([normalD; uniformD]');
    
    %% Compute densities
    uniDensity = 1 / ((upper - lower) ^ dim);
    
    trueValues = normP .* mvnpdf(data.observations, mu, variance) + ...
        uniP  .* (ones(N, 1) * uniDensity);
    
end

