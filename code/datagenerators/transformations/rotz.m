function [ r ] = rotz( angle )
    %ROTZ generates the non-homogenous rotation matrix necessary for rotation
    %along the `-axis in three dimensions
    %   INPUT
    %   - Angle: the angle of the rotation in degrees
    %   OUTPUT
    %   - r: The rotation matrix
    
    angle = angle * (pi / 180);
    
        r = [cos(angle)    , -sin(angle)    , 0     , 0;...
            sin(angle)     , cos(angle)     , 0     , 0;...
            0              , 0              , 1     , 0;
            0              ,0               , 0     , 1]; 
end

