function [ r ] = roty( angle )
    %ROTY generates the non-homogenous rotation matrix necessary for rotation
    %along the y-axis in three dimensions
    %   INPUT
    %   - Angle: the angle of the rotation in degrees
    %   OUTPUT
    %   - r: The rotation matrix
    
    angle = angle * (pi / 180);
    
    r = [cos(angle)    , 0         , sin(angle)    ,0;...
        0              , 1         , 0             ,0;...
        -sin(angle)    , 0         , cos(angle)    ,0;...
        0              , 0         , 0             ,1]; 
end

