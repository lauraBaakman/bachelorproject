function [ t ] = translate( deltas )
    %TRANSLATE Generate the transaltion matrix to move to delta
    %   INPUT
    %   - deltas: A column vector with three elements denoting how mcuh to
    %   move in the x, y and z-direction
    %   OUTPUT
    %   - A homogenous transformation matrix
    
    t = [diag(ones(3,1)), deltas;...
         0, 0, 0, 1];
 
end

