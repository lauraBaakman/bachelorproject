function [ data ] = transform(data, r)
    %TRANSFORM applies the transformation matrix t to each row in data
    %   INPUT
    %   data: A matrix with N rows and three columns
    %   r: A 3x3 transformation matrix
    %   OUTPUT
    %   data: The inputted data transformed according to the transformation
    %   matrix.
    
    [N, ~] = size(data);
    homData = [data, ones(N, 1)]';
    for i = 1:N
        homData(:,i) = r * homData(:,i);
    end
    
    data = homData(1:3,:)';    
end

