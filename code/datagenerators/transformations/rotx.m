function [ r ] = rotx( angle )
    %ROTX generates the homogenous rotation matrix necessary for rotation
    %along the x-axis in three dimensions
    %   INPUT
    %   - Angle: the angle of the rotation in degrees
    %   OUTPUT
    %   - r: The rotation matrix
    
    angle = angle * (pi / 180);
    
        r = [1             , 0          , 0				,0; ...
            0              , cos(angle) , -sin(angle)	,0; ...
            0              , sin(angle) , cos(angle)	,0; 
            0              ,0           , 0             ,1]; 
end

