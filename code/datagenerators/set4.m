function [ data, trueValues ] = set4( N )
    %FERDDATASET1 Generates N datapoints for dataset 1 (a combination of a trivariate gausian and uniform random noise) as defined by Ferdosi et al. Two thirds of the points will be drawn from the Gaussian, the rest from the uniform distribution.
    %   INPUT
    %   - N       The number of points to generate
    %   OUTPUT
    %   - data       A data object
    %   - trueValues The true densities of these points
    
    
    %% Init
    dim = 3;
    
    uniP = 1/5;
    wallP = 4/5;
    
    uniformN = ceil(N * uniP);
    wallN  = ceil(N * wallP);
    
    
    %% The wall data
    % parameters
    lowerWall = [0,0,14.5];
    upperWall = [30,30,15.5];

    
    % values
    [wallD] = blockUniformDist(lowerWall, upperWall, wallN, dim);
    
    %% The uniform data
    % parameters
    lower = 0;
    upper = 30;
    
    % values
    [uniformD] = uniformDist(lower, upper, uniformN, dim);
    
    %% Combine uniform and normal data
    data = dataObject([wallD; uniformD]');

   tempdata=cat(1,wallD,uniformD);
    
    %% Compute densities
    uniDensity = 1 / ((upper - lower) ^ dim);

    wallDensity = 1;
    for i=1 : dim
       wallDensity = wallDensity/(upperWall(i)-lowerWall(i)); 
    end
    wallSpace = (tempdata(:,3) < upperWall(3)) & (tempdata(:,3) > lowerWall(3));

    trueValues = wallP * wallDensity .* wallSpace  + ...
        uniP * uniDensity .* (ones(N, 1));
end

