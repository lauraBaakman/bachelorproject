function [ data, trueValues ] = set2( N )
    %SET2 scaled version of dataset 2 by Ferdosi: 2 multivariate gaussians + noise    
    %   INPUT
    %   - N       The number of points to generate
    %   OUTPUT
    %   - data       A data object
    %   - trueValues The true densities of these points
    
    %% Init
    dim = 3;
    
    uniP = 1/3;
    norm25P = 1/3;
    norm65P = 1/3;
    
    uniformN = ceil(N * uniP);
    normal25N  = ceil(N * norm25P);
    normal65N  = ceil(N * norm65P);
    
    
    %% The small normal data
    % parameters
    mu25 = 12.5 * ones(1, dim);
    variance25 = diag(5 * ones(dim,1));
    
    % values
    [normal25D] = normalDist(mu25, variance25, normal25N);    
    
    %% The large normal data
    % parameters
    mu65 = 32.5 * ones(1, dim);
    variance65 = diag(10 * ones(dim,1));
    
    % values
    [normal65D] = normalDist(mu65, variance65, normal65N);        
    %% The uniform data    
    % parameters
    lower = 0;
    upper = 50;
    
    % values
    [uniformD] = uniformDist(lower, upper, uniformN, dim);    
    
    uniDensity = 1 / ((upper - lower) ^ dim);
    
    %% Combine to one dataobject
    data = dataObject([normal25D; normal65D; uniformD]');
    trueValues = norm25P .* mvnpdf(data.observations, mu25, variance25) + ...
        norm65P .* mvnpdf(data.observations, mu65, variance65) + ...
        uniP  .* (ones(N, 1) * uniDensity);

        
end

