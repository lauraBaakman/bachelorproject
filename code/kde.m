function [computedDensities] = kde(data, N, mbe)
    %KDE performs kernel densities estimation using the (Adapated) Modified Breiman Estimator
    % INPUT
    %   - data: dataObject that contains the data to be used for KDE.
    %   -N: The number of datapoints to consider during the KDE.
    %   -mbe: True if the MBE is to be used, false if the AMBE is to be
    %   used.
    %
    % OUTPUT
    %   - MSE:  Mean Squared Error of the kde.
    %   - MSES: The MSE per datapoint
    %   - computedDensities: The densities computed by the estimator per
    %   data point
    %   - trueDensities: The true densities of these points
    
    
    %%Include necessary methods
    addpath('sigmaFunctions');
    addpath('kernels');
    addpath('errorMeasures');
    
    %% Parameters
    lowerPercentile = 20;
    upperPercentile = 80;
    alpha = 1/5; %Alternatives: 1/data.dim of 0.5. Parzen: 1/5
    
    %% Compute optimal sigma
    fprintf('\t\tComputing sigma_opt..\n')
    sigmaFunc = @(x) percentile(x, lowerPercentile, upperPercentile);
    sigmaOpt = optimalPilotWindow(data, sigmaFunc);
    
    %% Compute pilot densities
    fprintf('\t\tComputing pilot densities..\n')
    lambdas = ones(data.N, 1);
    pilotDensities = estimateDensities(data, lambdas, sigmaOpt, true);
    
    %% Compute local bandwidth parameters
    fprintf('\t\tComputing local bandwidth parameters..\n')
    lambdas = localBandwidthParameters( pilotDensities, alpha);
    
    %% Compute the densities
    fprintf('\t\tComputing densities..\n')
    computedDensities = estimateDensities(data, lambdas, sigmaOpt, mbe);
end

