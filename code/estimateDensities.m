function [densities] = estimateDensities(data, gammas, sigmaOpt, shapeConsistent)
    %ESTIMATEDENSITIES estimates the densities for each observation of the inputted data
    %object. If the kernels argument is not used a normal kernel with mean
    %zero and covariance matrix diag(sigmaOpt) is used.
    %   INPUT
    %   - data: A (N x d) data-object
    %   - sigmaOpt: A scalar with the optimal window width
    %   - gammas: A
    %   - shapeConsistent: If true no shape adaptive kernels are used
    %   OUTPUT
    %   - density: Estimated densities
    
    densities = zeros(data.N, 1);
    if(shapeConsistent)
    % Pilot densities or final densities for MBE
        % Compute for each observation in data a density estimate
        for i = 1:data.N
            r = data.getObservation(i);
           densities(i) = estimateDensity(r, data, sigmaOpt, gammas);
        end
    else
    % Final density for AMBE
        % Compute for each observation in data a density estimate
        for i = 1:data.N
           r = data.getObservation(i);
           densities(i) = estimateShapeAdaptiveDensity(r, data, sigmaOpt, gammas);
        end
    end    
end

function [density] = estimateDensity(r, data, sigmaOpt, gammas)
    % Preallocate
    densities = zeros(data.N, 1);
    
    for i = 1:data.N
       ri = data.getObservation(i);
       
       % Density estimation for observation r based on observation ri
       densities(i) = mvnpdf(ri, r, diag(repmat((sigmaOpt * gammas(i))^2, 3, 1)));
    end
    density = mean(densities);
end

function [density] = estimateShapeAdaptiveDensity(r, data, sigmaOpt, gammas)
    % Pre allocate
     densities = zeros(data.N, 1);

    basisSigma = computeKernelShape(r, data, sigmaOpt);
     
     for i = 1:data.N
        ri = data.getObservation(i);
        
        % Compute kernel shape
        SIGMA = basisSigma * gammas(i) * gammas(i);
        
        % Density estimation for observation r based on observation ri
        densities(i) = mvnpdf(ri, r, SIGMA);
     end
     density = mean(densities);
end


function [ kernelShape ] = computeKernelShape(observation, data, width)
    %COMPUTEKERNELSHAPE computes the shape of the kernel
    %   INPUT
    %   - observation: The observation to for which the kernel shapes needs
    %   to be determined.
    %   - data: Dataobject with the data to be used.
    %   - sigmaOpt: The optimal sigma
    %   OUTPUT
    %   - The kernelshape
    
        % The number of points to consider
        k = ceil(sqrt(data.N)); % Silverman: k \approx sqrt(n)
        
        % The distance method to be used
        distanceMethod = 'euclidean';
    
        % Determine points to take into consideration
        indices = knnsearch(data.observations, observation, 'k', k, 'distance', distanceMethod);
        
        % Get the actual points
        points = data.getObservation(indices);
        
        % Compute the covariance matrix
        covMat = cov(points);
        
        % Compute the eigenvalues
        eigenValues = eig(covMat);
        
         % Compute the scaling factor
        S = scalingFactor(eigenValues, width^2);
        
        % Compute variance matrix
        kernelShape = S * covMat;
    
end

function S = scalingFactor(eigenValues, width)
    %SCALINGFACTOR Computes the scaling factor for the covariance matrix
    %that is used to form the kernel.
    % INPUT
    %   - lambdas:  The eigenvalues of the covariance matrix of the
    %   datapoints to take into consideration.
    %   - sigmaOpt: The optimal sigma
    
    S = width / geomean(sqrt(eigenValues));    
end

